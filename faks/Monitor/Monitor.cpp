#include "Monitor.hpp"
#include <iostream>

using namespace std;

Monitor::Monitor() {
    state = sOFF;
    brightness = 0;
}

Monitor::Monitor(const Monitor &m) {
    state = m.state;
    brightness = m.brightness;
}

bool Monitor::turnOn() {
    bool uspesno = false;
    if (state == sTEST || state == sOFF || state == sSTANDBY){
        state = sON;
        uspesno = true;
    }

    return uspesno;
}

bool Monitor::turnOff() {
    bool uspesno = false;
    if (state == sTEST || state == sON || state == sSTANDBY){
        state = sOFF;
        brightness = 0;
        uspesno = true;
    }

    return uspesno;
}


bool Monitor::turnOut() {
    bool uspesno = false;
    if (state == sTEST || state == sON || state == sSTANDBY || state == sOFF){
        state = sOUT;
        brightness = -1;
        uspesno = true;
    }

    return uspesno;
}

bool Monitor::repair() {
    bool uspesno = false;
    if (state == sOUT){
        state = sOFF;
        brightness = 0;
        uspesno = true;
    }

    return uspesno;
}

bool Monitor::turnTest() {
    bool uspesno = false;
    if (state == sON){
        state = sTEST;
        uspesno = true;
    }

    return uspesno;
}

bool Monitor::turnStandBy() {
    bool uspesno = false;
    if (state == sON){
        state = sSTANDBY;
        uspesno = true;
    }

    return uspesno;
}


bool Monitor::incB() {
    bool uspesno = false;
    if (state == sON || state == sTEST){
        if(brightness + BRIGHTNESS_STEP <= BRIGHTNESS_MAX){
            brightness += 2;
            uspesno = true;
        }
    }

    return uspesno;
}

bool Monitor::decB() {
    bool uspesno = false;
    if (state == sON || state == sTEST){
        if(brightness - BRIGHTNESS_STEP >= BRIGHTNESS_MIN){
            brightness -= 2;
            uspesno = true;
        }
    }

    return uspesno;
}




void Monitor::print() const{
    string stateString;
    // {sON, sOFF, sOUT, sTEST, sSTANDBY};
    switch(state){
        case 0: stateString = "sON"; break;
        case 1: stateString = "sOFF"; break;
        case 2: stateString = "sOUT"; break;
        case 3: stateString = "sTEST"; break;
        case 4: stateString = "sSTANDBY"; break;
    }
    cout<<"Trenutno stanje: "<<stateString<<endl;
    cout<<"Trenutno osvetljenje: "<<brightness<<endl;
}

#include "Monitor.hpp"

#include <iostream>

using namespace std;

int meni(){
    int unos = 0;

    cout<<"------------------------------"<<endl;
    cout<<"1. turnOn()"<<endl;
    cout<<"2. turnOff()"<<endl;
    cout<<"3. turnOut()"<<endl;
    cout<<"4. repair()"<<endl;
    cout<<"5. turnTest()"<<endl;
    cout<<"6. turnStandBy()"<<endl;
    cout<<"7. incB()"<<endl;
    cout<<"8. decB()"<<endl;
    cout<<"9. print()"<<endl;
    cout<<"10. TEST KONSTRUKTORA KOPIJE!"<<endl;
    cout<<"11. IZLAZ!"<<endl;
    cout<<"------------------------------"<<endl;


    cin>>unos;

    return unos;
}

int main()
{
    int unos = 0;

    // konstruktor bez parametara
    Monitor m;

    // Inicijalizacija konstruktora kopije
    Monitor kopija(m);

    do {
        unos = meni();

    switch(unos){
    case 1:
        if(m.turnOn()) {
            cout<<"Uspesno UKLJUCENO!"<<endl;
        } else {
            cout<<"Operacija nije uspesna!"<<endl;
        }
    break;

    case 2:
        if(m.turnOff()) {
            cout<<"Uspesno ISKLJUCENO!"<<endl;
        } else {
            cout<<"Operacija nije uspesna!"<<endl;
        }
    break;

    case 3:
        if(m.turnOut()) {
            cout<<"Uspesan prelazak u sOUT!"<<endl;
        } else {
            cout<<"Operacija nije uspesna!"<<endl;
        }
    break;

    case 4:
        if(m.repair()) {
            cout<<"Uspesno POPRAVLJEN!"<<endl;
        } else {
            cout<<"Operacija nije uspesna!"<<endl;
        }
    break;

    case 5:
        if(m.turnTest()) {
            cout<<"Uspesno aktiviran TEST!"<<endl;
        } else {
            cout<<"Operacija nije uspesna!"<<endl;
        }
    break;

    case 6:
        if(m.turnStandBy()) {
            cout<<"Uspesno aktiviran STAND BY!"<<endl;
        } else {
            cout<<"Operacija nije uspesna!"<<endl;
        }
    break;

    case 7:
        if(m.incB()) {
            cout<<"Uspesno ucevan BRIGHTNESS!"<<endl;
        } else {
            cout<<"Operacija nije uspesna!"<<endl;
        }
    break;

    case 8:
        if(m.decB()) {
            cout<<"Uspesno umanjen BRIGHTNESS!"<<endl;
        } else {
            cout<<"Operacija nije uspesna!"<<endl;
        }
    break;

    case 9:
        m.print();
    break;

    case 10:
        cout<<"Test za konstruktor kopije..."<<endl;
        cout<<"-----------------------------"<<endl;
        kopija.print();
        cout<<"-----------------------------"<<endl;
    break;

    // za nepredvidjeno stanje izadji iz programa
    default:
        return 0;
    break;
    }

    m.print();
    } while(unos != 11);

    return 0;
}

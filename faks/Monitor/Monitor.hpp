#ifndef MONITOR_HPP_INCLUDED
#define MONITOR_HPP_INCLUDED



#endif // MONITOR_HPP_INCLUDED

#define BRIGHTNESS_MIN 0
#define BRIGHTNESS_MAX 20
#define BRIGHTNESS_STEP 2
// sON, sOFF, sOUT, sTEST i sSTANDBY.
enum StateType {sON, sOFF, sOUT, sTEST, sSTANDBY};

class Monitor{
private:
    StateType state;
    int brightness;

    public:

    // Klasa treba da sadrži konstruktor bez parametara koji postavlja objekat u stanje
    // sOFF, a brightness na 0
    Monitor();

    // Klasa treba da sadrži i konstruktor kopije.
    Monitor(const Monitor &);

    // Brightness se može menjati samo kada je monitor u stanju sON ili sTEST, sa korakom 2.

    bool turnOn();
    bool turnOff();
    bool turnOut();
    bool repair();
    bool turnTest();
    bool turnStandBy();
    bool incB();
    bool decB();
    void print() const;
//    IMPLEMENTIRATI:
//- prazan konstruktor=============
//- konstruktor kopije ============
//- metodu bool turnOn()
//- metodu bool turnOff()
//- metodu bool turnOut()
//- metodu bool repair()
//- metodu bool turnTest()
//- metodu bool turnStandBy()
//- metodu bool incB()
//- metodu bool decB()
//- metodu void print() const
};


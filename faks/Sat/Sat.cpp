#include "Sat.hpp"

using namespace std;

bool Sat::postaviSekunde(int s){

}

bool Sat::postaviMinute(int m){

}

bool Sat::postaviSate(int h){

}

bool Sat::dodajSekunde(int s){

}

bool Sat::dodajMinute(int m){

}

bool Sat::dodajSate(int h){

}

int Sat::brojSekundi(int s) const{

}

int Sat::brojMinuta(int m) const{

}

int Sat::brojSati(int h) const{

}

Sat::Sat(){
    sekunde     = 0;
    minuti      = 0;
    sati        = 0;
}

Sat::Sat(const Sat &s1){
    sekunde     = s1.sekunde;
    minuti      = s1.minuti;
    sati        = s1.sati;
}

Sat::Sat(int s){
    sekunde     = s;
    minuti      = 0;
    sati        = 0;
}

Sat::Sat(int s, int m){
    sekunde     = s;
    minuti      = m;
    sati        = 0;
}

Sat::Sat(int s, int m , int h){
    sekunde     = s;
    minuti      = m;
    sati        = h;
}

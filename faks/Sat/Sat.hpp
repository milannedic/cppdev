#ifndef SAT_CPP_INCLUDED
#define SAT_CPP_INCLUDED

#include <iostream>
using namespace std;

class Sat {
    private:
        int sati;
        int minuti;
        int sekunde;
    public:

    bool postaviSekunde(int);
    bool postaviMinute(int);
    bool postaviSate(int);

    bool dodajSekunde(int);
    bool dodajMinute(int);
    bool dodajSate(int);

    int brojSekundi(int) const;
    int brojMinuta(int) const;
    int brojSati(int) const;

    Sat();
    Sat(const Sat &);
    Sat(int);
    Sat(int, int);
    Sat(int, int, int);

    Sat& operator=(const Sat&);
    //Sat& operator+=(const Sat&);
    //Sat& operator-=(const Sat&);

    //Sat& operator++(const Sat&);
    //Sat& operator--(const Sat&);

//- referenca klase predstavlja povratnu vrednost sledećih preklopljenih
//
//operatora =, +=, -=, prefiksni -- i ++
//
//- bool predstavlja povratnu vrednost preklopljenih operatora za proveru
//
//jednakosti: == i !=
//
//- vrednost klase predstavlja povratnu vrednost sledećih operatora:
//
//-, +, postfiksni -- i ++
//
//- operatori -- i ++ inkrementiraju/dekrementiraju sekunde
//
//- referenca ostream predstavlja povratnu vrednost za preklopljeni
//
//operator za rad sa konzolom <<
//
//- referenca istream predstavlja povratnu vrednost za preklopljeni
//
//operator za rad sa tastaturom >>
//
//- voditi računa o granicama: vrednosti za sva polja ne mogu biti manja
//
//od 0, vrednosti za sekunde i minute ne mogu biti veće od 59, a sati
//
//mogu imati bilo koju celobrojnu nenegativnu vrednost

};



#endif // SAT_CPP_INCLUDED

#ifndef POLINOM_HPP_INCLUDED
#define POLINOM_HPP_INCLUDED

#include <iostream>
using namespace std;

class Polinom {
    private:
        double *a;
        int n;
    public:
        Polinom() { a = NULL; n = -1;}
        Polinom(const double[], int n);
        Polinom(const Polinom &);
        ~Polinom() { delete [] a;}
        int getN() const { return n;}
        double& operator[](int);
        double operator[](int) const;
        double operator()(double) const;
        Polinom& operator=(const Polinom&);
        Polinom& operator+=(const Polinom&);
        Polinom& operator-=(const Polinom&);

        friend bool operator==(const Polinom&, const Polinom&);
        friend bool operator!=(const Polinom&, const Polinom&);

        friend Polinom operator-(const Polinom&);                       // Unarna operacija nad polinomom, -p
        friend Polinom operator+(const Polinom&, const Polinom&);       // Oduzima drugi od prvog
        friend Polinom operator-(const Polinom&, const Polinom&);

        friend ostream& operator<<(ostream&, const Polinom&);

};


#endif // POLINOM_HPP_INCLUDED

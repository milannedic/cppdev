#include <iostream>
#include "Polinom.hpp"

using namespace std;

int main()
{
    const double d1[] = {0.1, 0.2, 0.3, 0.4};
    const double d2[] = {0.5, 0.6, 0.7, 0.8, 0.9};
    const double d3[] = {4, 3, 2, 1, 0, -1};

    Polinom p0, p1(d1, 3), p2(d2, 4), p3(d3, 5);

    cout << p0 << endl;
    cout << p1 << endl;
    cout << p2 << endl;
    cout << p3 << endl;

    cout << p0(1) << endl;
    cout << p1(0.7) << endl;
    cout << p2(3) << endl;
    cout << p3(2) << endl;

    p0 = p1 + p2;
    cout << p0 << endl;

    p0 = p1 - p2;
    cout << p0 << endl;

    p0 = p2 - p1;
    cout << p0 << endl;

    p1 = p0;

    cout << (p1 == p0) << endl;
    cout << (p2 == p0) << endl;

    p1 += p3;
    cout << p1 << endl;

     p1 -= p3;
    cout << p1 << endl;

    return 0;
}

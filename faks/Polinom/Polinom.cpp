#include "Polinom.hpp"

Polinom::Polinom(const double niz[], int red){
    n = red;
    a = new double[n + 1];
    for(int i = 0; i <= n; i++){
        a[i] = niz[i];          // Koeficijenti polinoma.
    }
}

Polinom::Polinom(const Polinom &p){
    n = p.n;
    a = new double[n + 1];
    for(int i = 0; i <= n; i++){
        a[i] = p.a[i];          // Koeficijenti polinoma.
    }
}

double& Polinom::operator[](int i) {
    cout << "&&& " << a+i << endl;
    //return a[i];
    return *(a+i);
}

double Polinom::operator[](int i) const {
    //cout << "###" << a[i] << endl;
    //return a[i];
    return *(a+i);
}

/// NEJASNA METODA
double Polinom::operator()(double x) const {
    double s = 0;
    if(n >= 0){
        s = a[n];                           // Krenem od najveceg koeficijenta
        for(int i = n-1; i >= 0; i--){
            s = s*x + a[i];
        }
    }

    return s;
}

Polinom& Polinom::operator=(const Polinom &p){
    /// this je objekat u kojem se trenutno nalazim, &p je referenca na neki objekat
    /// i ovde se vrsi poredjenje objekat != objekat
    if(this != &p){
        delete [] a;
        n = p.n;
        a = new double [n + 1];

        for(int i = 0; i <= n; i++){
            a[i] = p.a[i];
        }
    }

    return *this;
}

Polinom& Polinom::operator+=(const Polinom &p){
    /// STA SE OVDE DESILO???
    *this = *this + p;
    return *this;
}


Polinom& Polinom::operator-=(const Polinom &p){
    /// STA SE OVDE DESILO???
    *this = *this - p;
    return *this;
}

bool operator==(const Polinom &p, const Polinom &q){
   /// Ako nisu istog reda, sigurno nisu isti
   if(p.n != q.n){
        return false;
   }

   /// Ako im se neki koeficijent razlikuje, nisu isti!
   for(int i = 0; i<= p.n; i++){
        if(p.a[i] != q.a[i]){
            return false;
        }
   }

   return true;
}


bool operator!=(const Polinom &p, const Polinom &q){
   /// Ako nisu istog reda, sigurno nisu isti
   if(p.n != q.n){
        return true;
   }

   /// Ako im se neki koeficijent razlikuje, nisu isti!
   for(int i = 0; i<= p.n; i++){
        if(p.a[i] != q.a[i]){
            return true;
        }
   }

   return false;
}

Polinom operator-(const Polinom &p){
   Polinom r;
   if(p.n > 0){
        r.n = p.n;
        r.a = new double[r.n+1];
        for(int i = 0; i <= p.n; i++){
            r.a[i] = - p.a[i];
        }
   }
   return r;
}

Polinom operator+(const Polinom &p, const Polinom &q){
   int i;
   Polinom r;
   if(p.n <= q.n){
       r = q;
       for(int i = 0; i < p.n; i++){
            r[i] += p[i];
       }
   } else {
       r = p;
       for(int i = 0; i < q.n; i++){
            r[i] += q[i];
       }
   }
   while(r.a[r.n] == 0){
        r.n--;
   }
   return r;
}


Polinom operator-(const Polinom &p, const Polinom &q){
   Polinom r;
   r = p + (-q);        // Preklopljeno sabiranje dva polinoma i negacija jednog polinoma
   return r;
}

ostream& operator<<(ostream &out, const Polinom &p){
    out << "[";
    if(p.n >= 0){
        for(int i = 0; i < p.n; i++){
            out << " " << p.a[i] << ",";
            out << " " << p.a[p.n] << " ]";
        }
    } else {
        out<<" ]";
    }
    return out;
}


#include <iostream>
#include "Auto.hpp"

void print(const Auto &a) {
    cout << "----------------------" << endl;
    cout << "Instanci: " << a.getBrInstanci() << endl;
    cout << "Marka: " << a.getMarka() << endl;
    cout << "Model: " << a.getModel() << endl;
    cout << "Gorivo: " << a.getGorivo() << endl;
    cout << "Jacina: " << a.getJacina() << endl;
    cout << "----------------------" << endl;
}

using namespace std;

int main()
{
    Auto a;
    Auto b(a);

    Auto c("VW", "Golf R32", 3.2 , "Benzin");
    Auto d("VW", "Golf TDI", 1.9 , "Dizel");

    print(a);
    print(b);
    print(c);
    print(d);

    return 0;
}

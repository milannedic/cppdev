#ifndef MASINA_HPP_INCLUDED
#define MASINA_HPP_INCLUDED

#include "dinstring.hpp"


class Masina {

    private:

    public:

    virtual DinString getMarka() const = 0;
    virtual DinString getModel() const = 0;
    virtual DinString getGorivo() const = 0;
    virtual double getJacina() const = 0;

    virtual void setMarka(DinString &) = 0;
    virtual void setModel(DinString &) = 0;
    virtual void setGorivo(DinString &) = 0;
    virtual void setJacina(double) = 0;
//
//    virtual void setMarka() = 0;
//    virtual void setModel() = 0;
//    virtual void setGorivo() = 0;
//    virtual void setJacina() = 0;

};

#endif // MASINA_HPP_INCLUDED

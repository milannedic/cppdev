#ifndef AUTO_HPP_INCLUDED
#define AUTO_HPP_INCLUDED

#include "Masina.hpp"

class Auto: public Masina {
    private:

    DinString model, marka, gorivo;
    double jacina;
    static int brojInstanci;

    public:

    Auto() {
        brojInstanci++;

        marka = "VW";
        model = "Golf VR6";
        gorivo = "Benzin";
        jacina = 2.9;
    }

    /// Konstruktor sa 4 parametra.
    Auto(DinString &m, DinString &mk, double pw, DinString &g)
    : model(m), marka(mk), gorivo(g), jacina(pw) {
        brojInstanci++;
    }

    /// Konstruktor sa 4 parametra koji nije TRAZEN!
    Auto(const char *m, const char *mk, double pw, const char *g)
    : model(m), marka(mk), gorivo(g), jacina(pw) {
        brojInstanci++;
    }

    /// Konstruktor kopije
    Auto(const Auto &a)
    : model(a.model), marka(a.marka), gorivo(a.gorivo), jacina(a.jacina) {
        brojInstanci++;
    }

    ~Auto() {
        brojInstanci--;
    }

    int getBrInstanci() const {
        return brojInstanci;
    }

    DinString getMarka() const {
        return marka;
    }

    DinString getModel() const {
        return model;
    }

    DinString getGorivo() const {
        return gorivo;
    }

    double getJacina() const {
        return jacina;
    }

    /// SET METODE
    /// SET METODE
    /// SET METODE
    /// SET METODE
    void setMarka(DinString &s) {
        marka = s;
    }

    void setModel(DinString &s){
        model = s;
    }

    void setGorivo(DinString &s) {
        gorivo = s;
    }

    void setJacina(double power) {
        jacina = power;
    }

    //friend void print(const Auto);


};

#endif // AUTO_HPP_INCLUDED

#ifndef STUDENT_HPP_INCLUDED
#define STUDENT_HPP_INCLUDED

#include "Osoba.hpp"

class Student : public Osoba {
    protected:
        int brojIndeksa;
    public:
        Student() {
            brojIndeksa = 131;
        }

        Student(const char* s1, const char* s2, int brIndeksa)
        : Osoba(s1, s2), brojIndeksa(brIndeksa) {
            cout << "Student: Konstruktor 1." << endl;
        }

        Student(const DinString& s1, const DinString& s2, int brIndeksa)
        : Osoba(s1, s2), brojIndeksa(brIndeksa) {
            cout << "Student: Konstruktor 2." << endl;
        }

        Student(const Osoba& osoba, int brIndeksa)
        : Osoba(osoba), brojIndeksa(brIndeksa) {
            cout << "Student: Konstruktor 3." << endl;

        }
        Student(const Student&s) : Osoba((Osoba)s), brojIndeksa(s.brojIndeksa) {
            cout << "Student: Konstruktor 4." << endl;

        }

        ~Student() {
			cout << "Student: Destruktor." << endl;
		}

		void predstaviSe() {
            cout << "Ime je: " << ime << ", prezime je : " << prezime << ", INDEX : " << brojIndeksa << endl << endl;
        }

};


#endif // STUDENT_HPP_INCLUDED

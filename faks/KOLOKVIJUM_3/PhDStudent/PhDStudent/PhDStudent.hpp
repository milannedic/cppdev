#ifndef PHDSTUDENT_HPP_INCLUDED
#define PHDSTUDENT_HPP_INCLUDED

#include "Student.hpp"

class PhDStudent : public Student {
    private:
        double prosecnaOcena;
    public:

    PhDStudent() {
        prosecnaOcena = 9.35;
        cout << "PhDStudent: Konstruktor 1." << endl;

    }

    PhDStudent (const char* s1, const char* s2, int brIndeksa, double ocena)
    : Student(s1, s2, brIndeksa), prosecnaOcena(ocena){
        cout << "PhDStudent: Konstruktor 2." << endl;
    }

    PhDStudent (const DinString& s1, const DinString& s2, int brIndeksa, double ocena)
    : Student(s1, s2, brIndeksa), prosecnaOcena(ocena){
        cout << "PhDStudent: Konstruktor 3." << endl;
    }

    PhDStudent (const Osoba& s, int brIndeksa, double ocena)
    : Student(s, brIndeksa), prosecnaOcena(ocena) {
        cout << "PhDStudent: Konstruktor 4." << endl;
    }

    PhDStudent (const Student& s, double ocena)
    : Student(s),  prosecnaOcena(ocena) {
        cout << "PhDStudent: Konstruktor 5." << endl;
    }

//    PhDStudent (const PhDStudent& s): PhDStudent(s) {
//        cout << "PhDStudent: Konstruktor 5." << endl;
//    }

    PhDStudent (const PhDStudent& phds)
    : Student((Osoba)phds, phds.brojIndeksa), prosecnaOcena(phds.prosecnaOcena) {
        cout << "PhDStudent: Konstruktor 5." << endl;
    }

    ~PhDStudent() {
			cout << "PhDStudent: Destruktor." << endl;
    }

    void predstaviSe() {
        cout << "Ime je: " << ime << ", prezime je : " << endl;
        cout << prezime << ", INDEX : " << brojIndeksa << endl;
        cout << "Prosecna ocena : " << prosecnaOcena << endl << endl << endl;
    }

};

#endif // PHDSTUDENT_HPP_INCLUDED

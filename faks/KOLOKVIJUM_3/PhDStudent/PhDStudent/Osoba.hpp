#ifndef OSOBA_HPP_INCLUDED
#define OSOBA_HPP_INCLUDED

#include "dinstring.hpp"

class Osoba {
    protected:
        DinString ime;
        DinString prezime;
    public:
        Osoba() {
            ime = "Ime";
            prezime = "Prezime";
        }

/// OVI KONSTRUKTORI RADE ALI PISACU PREKO konstruktora inicijalizatora
//        Osoba(const char* s1, const char* s2) {
//            ime = s1;
//            prezime = s2;
//        }
//        Osoba(const DinString& s1, const DinString& s2) {
//            ime = s1;
//            prezime = s2;
//        }
//
//        Osoba(const Osoba &osoba) {
//            ime = osoba.ime;
//            prezime = osoba.prezime;
//        }

        Osoba(const char* s1, const char* s2) : ime(s1), prezime(s2) {
            cout << "Osoba: Konstruktor 1." << endl;
        }

        Osoba(const DinString& s1, const DinString& s2) : ime(s1), prezime(s2) {
            cout << "Osoba: Konstruktor 2." << endl;
        }

        Osoba(const Osoba &osoba) : ime(osoba.ime), prezime(osoba.prezime){
            cout << "Osoba: Konstruktor 2." << endl;
        }

        ~Osoba() {
            cout << "Osoba: Destruktor." << endl;
        }

        void predstaviSe() {
            cout << "Ime je: " << ime << ", prezime je : " << prezime << endl << endl;
        }

};

#endif // OSOBA_HPP_INCLUDED

#include <iostream>
#include "PhDStudent.hpp"

using namespace std;

int main()
{
    Osoba a("Milan", "Nedic");

    a.predstaviSe();

    cout << "-----------------------------------------" << endl;

    Student s("Lemi", "Nedic", 131);

    s.predstaviSe();


    cout << "-----------------------------------------" << endl;

    PhDStudent phds("mILAAAN", "Nedic", 131, 9.55);

    phds.predstaviSe();
    return 0;
}

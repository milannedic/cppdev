#include <iostream>
#include <cmath>
#include "Elipsa.hpp"

Elipsa::Elipsa(){
    x = 3;
    y = 2;
}

Elipsa::Elipsa(double xx, double yy){
    x = xx;
    y = yy;
}

Elipsa::Elipsa(const Elipsa &e){
    x = e.x;
    y = e.y;
}

double Elipsa::getP(){
    return x * y * M_PI;
}

double Elipsa::getO(){
    return M_PI * (3 * (x + y) - sqrt((3*x + y) * (x + y*y)));
}

double Elipsa::getX(){
    return x;
}

double Elipsa::getY(){
    return y;
}

#ifndef ELIPSA_HPP_INCLUDED
#define ELIPSA_HPP_INCLUDED

#include "Figura.hpp"

class Elipsa : public Figura {
    private:
        double x;
        double y;
    public:
        Elipsa(); // bez param
        Elipsa(double xx, double yy); //
        Elipsa(const Elipsa &e);

        // implementacija apstraktnih metoda
        // "naklon interfejsu"
        double getP();
        double getO();

        double getX();
        double getY();
};


#endif // ELIPSA_HPP_INCLUDED

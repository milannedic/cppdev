#ifndef PRAVOUGAONIK_HPP_INCLUDED
#define PRAVOUGAONIK_HPP_INCLUDED

#include "Figura.hpp"

class Pravougaonik : public Figura {
    private:
        double x;
        double y;
    public:
        Pravougaonik(); // bez param
        Pravougaonik(double xx, double yy); //
        Pravougaonik(const Pravougaonik &p);

        // implementacija apstraktnih metoda
        // "naklon interfejsu"
        double getP();
        double getO();

        double getX();
        double getY();
};


#endif // PRAVOUGAONIK_HPP_INCLUDED

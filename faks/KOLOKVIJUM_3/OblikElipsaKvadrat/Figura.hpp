#ifndef FIGURA_HPP_INCLUDED
#define FIGURA_HPP_INCLUDED

class Figura {
    private:
            virtual double getP() = 0;
            virtual double getO() = 0;
    public:


};

#endif // FIGURA_HPP_INCLUDED

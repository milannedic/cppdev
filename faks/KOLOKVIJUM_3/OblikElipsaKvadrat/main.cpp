#include <iostream>

#include "Oblik.hpp"

using namespace std;

int main()
{
    //cout << "Hello world!" << endl;
    Oblik a(5,3);
    Oblik b;

    cout << "Povrsina oblika a = " << a.getPovrsina() << endl;
    cout << "Obim oblika a = " << a.getObim() << endl;

    cout << endl << endl << endl;

    cout << "Povrsina oblika b = " << b.getPovrsina() << endl;
    cout << "Obim oblika b = " << b.getObim() << endl;
    return 0;
}

#include <iostream>
#include "Pravougaonik.hpp"

Pravougaonik::Pravougaonik(){
    x = 6;
    y = 4;
}

Pravougaonik::Pravougaonik(double xx, double yy){
    x = xx;
    y = yy;
}

Pravougaonik::Pravougaonik(const Pravougaonik &p){
    x = p.x;
    y = p.y;
}

double Pravougaonik::getP(){
    return x * y;
}

double Pravougaonik::getO(){
    return 2 * x + 2 * y;
}

double Pravougaonik::getX(){
    return x;
}

double Pravougaonik::getY(){
    return y;
}

#ifndef OBLIK_HPP_INCLUDED
#define OBLIK_HPP_INCLUDED

#include "Pravougaonik.hpp"
#include "Elipsa.hpp"

/// U main ubacujem samo Oblik.hpp jer su unutar njega ukljuceni
/// Pravougaonik.hpp i Elipsa.hpp koji zasebno u sebi ukljucuju Figura.hpp
/// Na taj nacin su svi uvezani.

class Oblik {
    private:
        Pravougaonik A;
        Elipsa B;
    public:
        // Poluose elipse su dvostruko manje od stranica pravougaonika.
        Oblik() : A(6, 4), B(3, 2) {}
        Oblik(double xx, double yy) : A(xx, yy), B(xx / 2, yy / 2) {}
        // Konstruktor kopije.
        Oblik(const Oblik &o) : A(o.A), B(o.B) {}

        /// PRAVOUGAONIK
        double getPPravougaonika(){
            return A.getP();
        }

        double getOPravougaonika(){
            return A.getO();
        }

        /// ELIPSA
        double getPElipse(){
            return B.getP();
        }

        double getOElipse(){
            return B.getO();
        }

        /// OBLIK
        double getPovrsina() {
            return A.getP() - B.getP();
        }

        double getObim() {
            return A.getO() + B.getO();
        }

};
#endif // OBLIK_HPP_INCLUDED

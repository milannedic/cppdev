#include <iostream>

//#include "Complex.hpp"            // Complex.hpp is included in Matrix.hpp
#include "Matrix.hpp"               // So it works when its omitted in main

using namespace std;

int main()
{
    Matrix m1;
    Matrix m2(3, 3);
    Matrix m3(3, 3);

    Complex z1(1,1);
    Complex z2(2,2);
    Complex z3(3,3);
    Complex z4(4,4);

    //Complex complexArr = {{z1, z2}, {z3, z4}};

    //m1.assignValues({{z1, z2}, {z3, z4}});
    //m1.assignValues();
    for(int i = 0; i < m2.getRowNum(); i++) {
        for(int j = 0; j < m2.getColNum(); j++) {
            // Overloaded assignment of Complex type
            m2.assignValues(i, j, m2.ComplexNum(i+1, j+1));
        }
    }

    for(int i = 0; i < m3.getRowNum(); i++) {
        for(int j = 0; j < m3.getColNum(); j++) {
            // Overloaded assignment of Complex type
            m3.assignValues(i, j, m3.ComplexNum(i+22, j+22));
        }
    }

    //m1.printInfo();
    //m2.printInfo();

    cout << "Ispis matrice m2:" << endl;
    cout << "==================" << endl;

    cout << m2 << endl;

    cout << "Ispis matrice m3:" << endl;
    cout << "==================" << endl;

    cout << m3 << endl;

     cout << "Ispis zbira matrica:" << endl;
    cout << "==================" << endl;

    cout << (m2+m3) << endl;

    /// ISPIS, test operatora INDEKSIRANJA
    cout << endl << endl << "Ispis nekog clana matrice (OPERATOR INDEKSIRANJA):" << endl;
    // User-friendly indexing
    cout << m2(1,1) << endl;

    /// /////////////////////////////////////////////////////////////
    /// /////////////////////////////////////////////////////////////
    /// Primjer koji "priblizava" koncept reference...
//    Complex zTest(22,22);
//    Complex zTest1(33,33);
//    Complex& zTestref = zTest;  // ovo je referenca na objekat
//    zTest1 = zTest;             // ovo je KOPIJA originala
//
//    zTest1.setReal(0);          // setujem nad kopijom
//    zTestref.setReal(5);        // a ovde nad referencom koja ce da izmjeni original
//
//
//    cout << "Po vrednosti : " << zTest << endl ;
    /// /////////////////////////////////////////////////////////////
    /// /////////////////////////////////////////////////////////////


    /// TEST OPERATORA subscript-DODELE
    m2(2,2) = zTest;

    cout << endl << endl << "Ispis DODELJENOG clana matrice:" << endl << endl;
    // User-friendly indexing
    cout << m2(2,2) << endl << endl << endl;

    cout << "Ispis matrice m2 nakon upotrebe operatora subscript-dodjele:" << endl;
    cout << "==================" << endl;
    cout << m2 << endl;

    cout << "Ispis matrice m2 nakon sto joj je dodeljena matrica m3:" << endl;
    cout << "==================" << endl;
    m2 = m3;
    cout << m2 << endl;
    return 0;
}

#include "Matrix.hpp"
#include <assert.h>


Matrix::Matrix(int numRow, int numCol) {
    rowNum = numRow;    // Initialize class fields
    colNum = numCol;    // Initialize class fields

    cMatrix = new Complex * [rowNum];              // This will allocate 'numRow' many 'Complex *'s
    if(cMatrix){
        for( int i = 0; i < colNum; i++) {
            cMatrix[i] = new Complex [colNum];
            assert(cMatrix[i]);
        }
    }
}

/*
 * DESTRUCTOR METHOD
 *
 * Specifying a function/procedure as inline inside a class is hinting to the
 * compiler that instead of creating code to call the function and pass parameters,
 * the contents of the function should be placed at the point of call.
 */
inline Matrix::~Matrix() {
  delete[] cMatrix;
}

Complex& Matrix::ComplexNum(int real, int imag) {

    Complex z(real, imag);
    ztemp = z;

    return ztemp;
}

Complex Matrix::getElement(int row, int col) const {
    return cMatrix[row][col];
}

// Return type is Complex number, NO NEED FOR POINTER, NOTHING'S MODIFIED
/// INDEXING A MATRIX POSITION, returns Complex number from indexed position.
Complex  Matrix::operator() (int row, int col) const {

    return cMatrix[row][col]; /// Same thing as return *(*(cMatrix + row) + col)
}

/// RETURNS A reference TO THE COMPLEX NUMBER PLACED ON THE INDEXED POSITION
// This is used to assign NEW COMPLEX NUMBER to indexed position,
// this operator is used at the same time with operator=, m(1,2) = z;
// So C++ knows what to assign
/// reference IS RETURNED SO THE POSITION CAN BE MODIFIED, ASSIGNMENT OPERATOR
/// WILL TAKE HIS ARGUMENT (const Complex&) AND WRITE IT TO
/// THIS POSITION (reference to original object in memory)!
Complex&  Matrix::operator() (int row, int col) {

    assignToRow = row;
    assignToCol = col;

    return cMatrix[row][col];
}

    /************************************************************************
    // Complex   Matrix::operator() (int row, int col) const
    // Complex&  Matrix::operator() (int row, int col)
    //
    // Functions have the exact same body, returns both time POSITION TO BE ASSIGNED
    // or POSITION TO BE ACCESSED...
    *************************************************************************/

// Return type is Matrix& because this is assignment operator
// this "function" returns REFERENCE to matrix which is being
// modified, reference to the original object which we modify...
/**************************************************
SPECIFIC VALUE Assignment operator
**************************************************/
Matrix&  Matrix::operator=(const Complex &z)  {
    /// da nije bio & onda bi napravio kopiju matrice, ovako vracam referencu
    /// isti objekat koji sam i dobio, samo je modifikovan

    cMatrix[assignToRow][assignToCol] = z;

    // Returns modified object
    return *this;
    /// this je pokazivac na objekat, * ga dereferencira i napravi objekat,
    /// Matrix& kaze da to i ocekuje (objekat), odnosno ocekuje referencu na objekat
}

/**************************************************
WHOLE-MATRIX Assignment operator
**************************************************/
Matrix&  Matrix::operator=(const Matrix &m)  {

    for(int i = 0; i < m.getRowNum(); i++){
        for(int j = 0; j < m.getColNum(); j++){

            cMatrix[i][j] = m(i, j);

            /****************************************
                CONCLUSION (Took me a while to figure out this one line above...):
                -- Be careful with loop limits and matrix dimensions

                cMatrix[i][j] = m(i, j);
                -- Operands of Complex type, operator= is on Complex numbers
                -- m(i,j) is a Matrix, operator() is Matrix::operator()
                -- cMatrix[i][j] is a Complex type 2D Array, so [][] access to the Complex type

            *****************************************/
        }
    }

    return *this;
}

/**************************************************
MATRIX Addition operator
**************************************************/
Matrix operator+(const Matrix &m1, const Matrix &m2){
    Matrix m(m1.getRowNum(), m1.getColNum());

    // Complex temp;

    for(int i = 0; i < m.getRowNum(); i++) {
        for(int j = 0; j < m.getColNum(); j++) {
            // Overloaded assignment of Complex type
            //temp = m1.getElement(i, j) + m2.getElement(i, j);
            //m.assignValues(i, j, temp);
            m(i,j) = m1(i,j) + m2(i,j);     // Overloaded operators on matrices! Yay!
        }
    }

    return m;
}

// Out stream operator overloaded
ostream& operator<<(ostream& out, const Matrix& m){
    out << endl << endl;

    out << "  __                  __" << endl;
    for(int i = 0; i < m.getRowNum(); i++) {
        out << "  |  ";
        for(int j = 0; j < m.getColNum(); j++) {
            // Overloaded assignment of Complex type
            out << m.getElement(i, j) << "  ";
            if (j+1 == m.getColNum()) {
                out << "|";
            }
        }
        out << endl;
    }
    out << "  --                  --";

    return out;
}



void Matrix::assignValues(int rowPos, int colPos, Complex &z) {
    // This will copy object z to the given position inside Complex matrix
    cMatrix[rowPos][colPos] = z;
}

int Matrix::getColNum() const {
    return colNum;
}

int Matrix::getRowNum() const {
    return rowNum;
}

void Matrix::printInfo(/*const Matrix &m*/) {
    cout << "Matrix info:" << endl;
    cout << "-------------------" << endl;
    cout << "Rows :     " << rowNum<< endl;
    cout << "Columns :  " << colNum<< endl << endl;
}

#ifndef MATRIX_HPP_INCLUDED
#define MATRIX_HPP_INCLUDED

// If this is set to 1, matrix dimensions will be indexed from zero
// Matrix(1,1) will return value placed on second column and second row
// Matrix(2,2) will be bottom-right element in 3x3 matrix.
#define INDEX_FROM_ZERO 0

using namespace std;
#include <iostream>
#include "Complex.hpp"

class Matrix {
    private:
        int colNum;
        int rowNum;

        // Auxiliary fields, the get modified when modification operator() is called.
        // For example m(1,2) , those fields are set to 1 and 2 respectively.
        int assignToRow;
        int assignToCol;

        // Array of pointers to pointers of Complex type
        Complex **cMatrix;

        // Temporary Complex number used for matrix values assignment
        Complex ztemp;

    public:

    // Constructor, allocates matrix of given dimensions.
    Matrix(int numRow = 5, int numCol = 5);

    // Matrix destructor
    ~Matrix();

    Complex& operator() (int row, int col);             // Subscript operator --> ASSIGNMENT
    Complex  operator() (int row, int col) const;       // Subscript operator --> ACCESS

    /************************************************************************
    // C++ will know what to do in this situation
    // Matrix(1,2) = z; // There are 2 operators: subscript-indexing and
    // assignment, this is modeling subscript-assignment, when this happens
    // ROW and COLUMN parameters will be taken from OPERATOR() and the
    // VALUE-TO-BE-ASSIGNED will be taken from operator=

    // Took me a while to figure out! :)
    *************************************************************************/

    // This operator returns a modified matrix after specific value assignment
    // This modifies indexed position in the matrix
    // Position is indexed using operator()
    Matrix& operator=(const Complex &); // SPECIFIC VALUE ASSIGNMENT
    Matrix& operator=(const Matrix &m); // WHOLE MATRIX ASSIGNMENT

    //void assignValues(Complex **matrix);
    void assignValues(int rowPos, int colPos, Complex &z);

    // Generates specific complex number
    Complex& ComplexNum(int real, int imag);

    // Returns matrix element from given position
    Complex  getElement(int row, int col) const;
    Complex& getElementAddress(int row, int col) const;

    // Auxiliary getter methods
    int getColNum() const;
    int getRowNum() const;

    friend Matrix operator+(const Matrix&, const Matrix&);
    friend ostream& operator<<(ostream& out, const Matrix& m);
    void printInfo();
};


#endif // MATRIX_HPP_INCLUDED

#include "Sat.hpp"

// prazan konstruktor
Sat::Sat(){
    sekunde = SEKUNDE_MINIMUM;
    minuti = MINUTI_MINIMUM;
    sati = SATI_MINIMUM;
}

// konstruktor sa parametirma: sekunde
Sat::Sat(int s){
    // vreme prvo mora da se postavi na 0:0:0
    // predstavlja pocetno vreme za postavljanje sekundi
    sekunde = SEKUNDE_MINIMUM;//ne ide s da ne bi korisnik dobio unazad sat koji ide ako da negativne vr
    minuti = MINUTI_MINIMUM;
    sati = SATI_MINIMUM;

    // pre postavljanja sekundi treba proveriti
    // da li je broj sekundi veci od ukupnog broja sekundi dana,
    // vreme ne moze imati negativne vrednosti
    if(s >= SEKUNDE_MINIMUM){
        dodajSekunde(s);//da korisnik preracuna koliko je to
    }
}

// konstruktor sa parametrima: sekunde, minuti
Sat::Sat(int s, int m){
    // vreme prvo mora da se postavi na 0:0:0
    // predstavlja pocetno vreme za postavljanje sekundi i minuta
    sekunde = SEKUNDE_MINIMUM;
    minuti = MINUTI_MINIMUM;
    sati = SATI_MINIMUM;

    // pre postavljanja sekundi i minuta treba proveriti
    // da li je broj sekundi + broj minuta izrazen u sekundama veci od ukupnog broja sekundi dana,
    // vreme ne moze imati negativne vrednosti

    if (s >= SEKUNDE_MINIMUM && m >= MINUTI_MINIMUM) {
        dodajSekunde(s);
        dodajMinute(m);
    }
}

// konstruktor sa parametrima: sekunde, minuti, sati
Sat::Sat(int s, int m, int st){
    // vreme prvo mora da se postavi na 0:0:0
    // predstavlja pocetno vreme za postavljanje sekundi, minuta i sati
    sekunde = SEKUNDE_MINIMUM;
    minuti = MINUTI_MINIMUM;
    sati = SATI_MINIMUM;

    // pre postavljanja sekundi, minuta i sati treba proveriti
    // da li je broj sekundi + broj minuta izrazen u sekundama + broj sati izrazen u sekundama veci od ukupnog broja sekundi dana,
    // vreme ne moze imati negativne vrednosti

    if (s >= SEKUNDE_MINIMUM && m >= MINUTI_MINIMUM && st >= SATI_MINIMUM) {
        dodajSekunde(s);
        dodajMinute(m);
        dodajSate(st);
    }
}

// konstruktor kopije
Sat::Sat(const Sat& sat){
    sekunde = sat.sekunde;
    minuti = sat.minuti;
    sati = sat.sati;
}

bool Sat::postaviSekunde(int s){
    // vreme ne moze da ima negativan broj sekundi ili broj sekundi veci od 59
    bool uspesnost;

   if(s < SEKUNDE_MINIMUM || s > SEKUNDE_MAKSIMUM){
        uspesnost = false;
    }else{
        sekunde = s;
        uspesnost = true;
    }

    return uspesnost;
}

bool Sat::postaviMinute(int m){
    // vreme ne moze da ima negativan broj minuta ili broj minuta veci od 59
    bool uspesnost;

    if(m < MINUTI_MINIMUM || m > MINUTI_MAKSIMUM){
        uspesnost = false;
    }else{
        minuti = m;
        uspesnost = true;
    }

    return uspesnost;
}

bool Sat::postaviSate(int s){
    // vreme ne moze da ima negativan broj Sati
    bool uspesnost;

    if(s < SATI_MINIMUM){
        uspesnost = false;
    }else{
        sati = s;
        uspesnost = true;
    }

    return uspesnost;
}

bool Sat::dodajSekunde(int s){
    bool uspesnost;

    int ukupnoSekundi = sati * 3600 + minuti * 60 + sekunde;

    if( (ukupnoSekundi + s) < SEKUNDE_MINIMUM ){
        uspesnost = false;
    } else {
        ukupnoSekundi += s;
        sati = ukupnoSekundi / 3600;
        minuti = (ukupnoSekundi % 3600) / 60;
        sekunde = (ukupnoSekundi % 3600) % 60;
        uspesnost = true;
    }
    return uspesnost;
}

bool Sat::dodajMinute(int m){
    return dodajSekunde(m * 60);
}

bool Sat::dodajSate(int s){
    return dodajSekunde(s * 3600);
}

int Sat::brojSekundi()const{
    return sekunde;
}

int Sat::brojMinuta()const{
    return minuti;
}

int Sat::brojSati()const{
    return sati;
}

Sat operator+(const Sat& sat1, const Sat& sat2){
    Sat sat(sat1.sekunde + sat2.sekunde, sat1.minuti + sat2.minuti, sat1.sati + sat2.sati);
    return sat;
}

Sat operator-(const Sat& sat1, const Sat& sat2){
    // provera da li je moguce oduzeti vreme vrsi se nad kopijom 1. parametra
    Sat sat(sat1);

    if (!sat.dodajSekunde(-(sat2.sekunde + sat2.minuti * 60 + sat2.sati * 3600))) {
        sat.postaviSekunde(0);
        sat.postaviMinute(0);
        sat.postaviSate(0);
    }

    return sat;
}

Sat& Sat::operator=(const Sat &sat){
    sekunde = sat.sekunde;
    minuti = sat.minuti;
    sati = sat.sati;
    return *this;
}

Sat& Sat::operator+=(const Sat &sat){
    // pravi se kopija trenutnog vremena
    // kopija je potrebna da bi se izvrsila provera da li je moguce sabiranje vremena
    Sat temp(sekunde, minuti, sati);
    if (temp.dodajSekunde(sat.sekunde + sat.minuti * 60 + sat.sati * 3600)){
        sekunde = temp.sekunde;
        minuti = temp.minuti;
        sati = temp.sati;
    }

    return *this;
}

Sat& Sat::operator-=(const Sat &sat){
    // pravi se kopija trenutnog vremena
    // kopija je potrebna da bi se izvrsila provera da li je moguce oduzimanje vremena
    Sat temp(sekunde, minuti, sati);
    if (temp.dodajSekunde(-(sat.sekunde + sat.minuti * 60 + sat.sati * 3600))){
        sekunde = temp.sekunde;
        minuti = temp.minuti;
        sati = temp.sati;
    }
    return *this;
}

const Sat& Sat::operator++(){
    dodajSekunde(1);
    return *this;
}

const Sat Sat::operator++(int){
    Sat sat(sekunde, minuti ,sati);
    dodajSekunde(1);
    return sat;
}

const Sat& Sat::operator--(){
    dodajSekunde(-1);
    return *this;
}

const Sat Sat::operator--(int){
    Sat sat(sekunde, minuti, sati);
    dodajSekunde(-1);
    return sat;
}

bool operator==(const Sat &sat1, const Sat &sat2){
    return sat1.sekunde == sat2.sekunde && sat1.minuti == sat2.minuti && sat1.sati == sat2.sati;
}

bool operator!=(const Sat &sat1, const Sat &sat2){
    return sat1.sekunde != sat2.sekunde || sat1.minuti != sat2.minuti || sat1.sati != sat2.sati;
}

ostream& operator<<(ostream &out, const Sat &sat){
    out << sat.sati << ":" << sat.minuti << ":" << sat.sekunde << endl;
    return out;
}

istream& operator>>(istream &in, Sat &sat){
    in >> sat.sati >> sat.minuti >> sat.sekunde;
    return in;
}

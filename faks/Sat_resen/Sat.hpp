#ifndef ZAGLAVLJE_HPP_INCLUDED
#define ZAGLAVLJE_HPP_INCLUDED

#include <iostream>
using namespace std;

#define SEKUNDE_MINIMUM 0
#define SEKUNDE_MAKSIMUM 59

#define MINUTI_MINIMUM 0
#define MINUTI_MAKSIMUM 59

#define SATI_MINIMUM 0

class Sat{
    private:
        int sekunde;
        int minuti;
        int sati;
    public:
        // prazan konstruktor
        Sat();
        //konstruktor sa parametirma: sekunde
        Sat(int);
        //konstruktor sa parametrima: sekunde, minuti
        Sat(int, int);
        //konstruktor sa parametrima: sekunde, minuti, sati
        Sat(int, int, int);
        // konstruktor kopije
        Sat(const Sat &);

        bool postaviSekunde(int);
        bool postaviMinute(int);
        bool postaviSate(int);

        bool dodajSekunde(int);
        bool dodajMinute(int);
        bool dodajSate(int);

        int brojSekundi()const;
        int brojMinuta()const;
        int brojSati()const;

        friend Sat operator+(const Sat&, const Sat&);
        friend Sat operator-(const Sat&, const Sat&);

        Sat& operator=(const Sat&);
        Sat& operator+=(const Sat&);
        Sat& operator-=(const Sat&);

        const Sat& operator++();
        const Sat operator++(int);
        const Sat& operator--();
        const Sat operator--(int);

        friend bool operator==(const Sat&, const Sat&);
        friend bool operator!=(const Sat&, const Sat&);

        friend ostream& operator<<(ostream&, const Sat&);
        friend istream& operator>>(istream&, Sat&);

};

#endif // ZAGLAVLJE_HPP_INCLUDED

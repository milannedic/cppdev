#include "Sat.hpp"

int main()
{
    Sat s1, s2(10), s3(20,30), s4(40, 50, 60), s5(s4);

    cout << "Sat 1: " << s1;
    cout << "Sat 2: " << s2;
    cout << "Sat 3: " << s3;
    cout << "Sat 4: " << s4;
    cout << "Sat 5: " << s5;

    cout << endl;

    // poziv preklopljenog operatora za proveru jednakosti
    cout << "Sat 4 == Sat 5 " << ((s4 == s5) ? "Da" : "Ne") << endl;

    // poziv preklopljenog operatora za proveru nejednakosti
    cout << "Sat 4 != Sat 5 " << ((s4 != s5) ? "Da" : "Ne") << endl;

    cout << endl;

    cout << "Sat 1: " << s1;
    if(s1.dodajSekunde(38)){
        cout << "Sat 1: dodajSekunde(38) Operacija izvrsena" << endl;
    }else{
        cout << "Sat 1: dodajSekunde(38) Operacija nije izvrsena" << endl;
    }

    cout << "Sat 1: " << s1;

    cout << endl;

    cout << "Sat 2: " << s2;
    if(s2.dodajMinute(60)){
        cout << "Sat 2: dodajMinute(60) Operacija izvrsena" << endl;
    }else{
        cout << "Sat 2: dodajMinute(60) Operacija nije izvrsena" << endl;
    }
    cout << "Sat 2: " << s2;

    cout << endl;

    cout << "Sat 3: " << s3;
    if(s3.dodajSate(13)){
        cout << "Sat 3: dodajSate(13) Operacija izvrsena" << endl;
    }else{
        cout << "Sat 3: dodajSate(13) Operacija nije izvrsena" << endl;
    }
    cout << "Sat 3: " << s3;

    cout << endl;

    cout << "Sat 1: " << s1;
    if(s1.dodajSekunde(42)){
        cout << "Sat 1: dodajSekunde(42) Operacija izvrsena" << endl;
    }else{
        cout << "Sat 1: dodajSekunde(42) Operacija nije izvrsena" << endl;
    }
    cout << "Sat 1: " << s1;

    cout << endl;

    cout << "Sat 2: " << s2;
    if(s2.dodajMinute(67)){
        cout << "Sat 2: dodajMinute(67) Operacija izvrsena" << endl;
    }else{
        cout << "Sat 2: dodajMinute(67) Operacija nije izvrsena" << endl;
    }

    if(s2.dodajMinute(67)){
        cout << "Sat 2: dodajMinute(67) Operacija izvrsena" << endl;
    }else{
        cout << "Sat 2: dodajMinute(67) Operacija nije izvrsena" << endl;
    }
    cout << "Sat 2: " << s2;

    cout << endl;

    cout << "Sat 3: " << s3;
    if(s3.dodajSate(22)){
        cout << "Sat 3: dodajSate(22) Operacija izvrsena" << endl;
    }else{
        cout << "Sat 3: dodajSate(22) Operacija nije izvrsena" << endl;
    }
    cout << "Sat 3: " << s3;

    cout << endl;

    cout << "Sat 4: " << s4;
    if(s4.dodajSekunde(-12)){
        cout << "Sat 4: dodajSekunde(-12) Operacija je izvrsena" << endl;
    }else{
        cout << "Sat 4: dodajSekunde(-12) Operacija nije izvrsena" << endl;
    }
    cout << "Sat 4: " << s4;


    cout << endl;

    cout << "Sat 4: " << s4;
    if(s4.dodajMinute(-67)){
        cout << "Sat 4: dodajMinute(-67) Operacija izvrsena" << endl;
    }else{
        cout << "Sat 4: dodajMinute(-67) Operacija nije izvrsena" << endl;
    }
    cout << "Sat 4: " << s4;


    cout << endl;

    cout << "Sat 4: " << s4;
    if(s4.dodajSate(-22)){
        cout << "Sat 4: dodajSate(-22) Operacija izvrsena" << endl;
    }else{
        cout << "Sat 4: dodajSate(-22) Operacija nije izvrsena" << endl;
    }
    cout << "Sat 4: " << s4;


    cout << endl;

    cout << "Sat 3: " << s3;
    cout << "Sat 3: getSati() " << s3.brojSati() << endl;
    cout << "Sat 3: getMinuti() " << s3.brojMinuta() << endl;
    cout << "Sat 3: getSekunde() " << s3.brojSekundi() << endl;

    cout << endl;

    cout << "Sat 1: " << s1;
    cout << "Sat 2: " << s2;
    cout << "Sat 1 = Sat 2" << endl;

    // poziv preklopljenog operatora dodele =
    s1 = s2;
    cout << "Sat 1: " << s1;
    cout << "Sat 2: " << s2;

    cout << endl;

    cout << "Sat 1: " << s1;
    // poziv preklopljenog operatora prefiksni ++
    cout << "Sat 1: ++s1 " << ++s1;
    cout << "Sat 1: " << s1;

    cout << endl;

    cout << "Sat 1: " << s1;
    // poziv preklopljenog operatora postfiskni ++
    cout << "Sat 1: s1++ " << s1++;
    cout << "Sat 1: " << s1;

    cout << endl;

    cout << "Sat 1: " << s1;
    // poziv preklopljenog operatora prefiksni --
    cout << "Sat 1: --s1 " << --s1;
    cout << "Sat 1: " << s1;

    cout << endl;

    cout << "Sat 1: " << s1;
    // poziv preklopljenog operatora postfiskni --
    cout << "Sat 1: s1-- " << s1--;
    cout << "Sat 1: " << s1;

    cout << endl;

    s1.postaviSate(0);
    s1.postaviMinute(0);
    s1.postaviSekunde(5);

    s2.postaviSate(0);
    s2.postaviMinute(0);
    s2.postaviSekunde(7);

    s3 = s1 - s2;
    cout << "Sat 1: " << s1;
    cout << "Sat 2: " << s2;
    cout << "s1- s2: " << s3;

    cout << endl;

    s1.postaviSate(0);
    s1.postaviMinute(8);
    s1.postaviSekunde(5);

    s2.postaviSate(0);
    s2.postaviMinute(11);
    s2.postaviSekunde(7);

    s3 = s1 - s2;
    cout << "Sat 1: " << s1;
    cout << "Sat 2: " << s2;
    cout << "s1- s2: " << s3;

    return 0;
}

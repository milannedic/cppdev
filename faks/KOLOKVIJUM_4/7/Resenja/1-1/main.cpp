#include <iostream>

#include "trezor.hpp"
#include "dijamant.hpp"

using namespace std;

int main()
{
    Trezor<Dijamant, 100> trezor;
    Dijamant d1, d2(100000, 50), d3(d2);

    cout << "D1: vrednost=" << d1.getVrednost() << " karat=" << d1.getKarat() << endl;
    cout << "D2: vrednost=" << d2.getVrednost() << " karat=" << d2.getKarat() << endl;
    cout << "D3: vrednost=" << d3.getVrednost() << " karat=" << d3.getKarat() << endl;

    cout << "Trezor: ubaci D1 rezultat=" << trezor.ubaciSadrzaj(d1) << endl;
    cout << "Trezor: broj popunjenih=" << trezor.getBrojPopunjenihSefova() << endl;
    cout << "Trezor: ubaci D2 rezultat=" << trezor.ubaciSadrzaj(d2) << endl;
    cout << "Trezor: broj popunjenih=" << trezor.getBrojPopunjenihSefova() << endl;
    cout << "Trezor: ubaci D3 rezultat=" << trezor.ubaciSadrzaj(d3) << endl;
    cout << "Trezor: broj popunjenih=" << trezor.getBrojPopunjenihSefova() << endl;

    cout << "Trezor: izbaci 50 rezultat=" << trezor.izbaciSadrzaj(50) << endl;
    cout << "Trezor: broj popunjenih=" << trezor.getBrojPopunjenihSefova() << endl;
    cout << "Trezor: izbaci 0 rezultat=" << trezor.izbaciSadrzaj(0) << endl;
    cout << "Trezor: broj popunjenih=" << trezor.getBrojPopunjenihSefova() << endl;

    return 0;
}

#ifndef TREZOR_HPP_INCLUDED
#define TREZOR_HPP_INCLUDED

template <class SADRZAJ, int KAPACITET>
class Trezor {

    private:
        SADRZAJ sefovi[KAPACITET];
        bool popunjenost[KAPACITET];

    public:
        Trezor() {
            for(int i = 0; i < KAPACITET; i++) {
                popunjenost[i] = false; // false ce nam oznacavati prazan sef, a true zauzet sef
            }
        }

        int getBrojPopunjenihSefova() {
            int n = 0;
            for(int i = 0; i < KAPACITET; i++) {
                if(popunjenost[i]) {
                    n++;
                }
            }
            return n;
        }

        int ubaciSadrzaj(const SADRZAJ& predmet) {
            int ret = -1;
            for(int i = 0; i < KAPACITET; i++) {
                if(!popunjenost[i]) {
                    popunjenost[i] = true;
                    sefovi[i] = predmet;
                    ret = i;
                    break;
                }
            }
            return ret;
        }

        bool izbaciSadrzaj(int sef) {
            bool ret = false;
            if(popunjenost[sef]) {
                popunjenost[sef] = false;
                ret = true;
            }
            return ret;
        }
};


#endif // TREZOR_HPP_INCLUDED

#ifndef NIZ_HPP_INCLUDED
#define NIZ_HPP_INCLUDED

#include <iostream>
using namespace std;

template <class T, int D>
class Niz {
    private:
        T el[D];
        int brEl;
    public:
        Niz() { brEl = 0; }
        ~Niz(){}
        int getBrEl() const {
            return brEl;
        }

        T operator[](int i) const { // ova metoda je const jer vraca indeks --> OPERATOR PRISTUPA
            return el[i];
        }

        T& operator[](int i) {      // vraca referencu, operator DODJELE
            return el[i];
        }

        Niz<T, D>& operator=(const Niz<T, D>&);
        void printNiz() const;
        bool insertNiz(const T&);
};

template <class T, int D>
Niz<T, D>& Niz<T, D>::operator=(const Niz<T, D> & rn) {
    // for(int brEl = 0; brEl < rn.brEl; brEl++)
    // ovaj int brEl u petlji kreira loklanu promenjivu koja
    // nema veze sa klasom i zato ne radi, kada ne kazem INT
    // onda se odnosi na POLJE KLASE.

    // RADI JER PRISTUPAM POLJU OBJEKTA IZ METODE KOJA PRIPADA KLASI
    // OD KOJE JE KREIRAN OVAJ OBJEKAT... PRIVATNIM POLJIMA SE PRISTUPA
    // SAMO PREKO METODA...
    for(brEl = 0; brEl < rn.brEl; brEl++){                // ZASTO OVDE MOGU PRISTUPITI PRIVATNOM POLJU?
    // for(brEl = 0; brEl < rn.getBrEl(); brEl++){
        el[brEl] = rn[brEl];
    }

    return *this;
}

template <class T, int D>
void Niz<T, D>::printNiz() const{
    cout << " ( " ;
    for(int i = 0; i < brEl - 1; i++ ) {
        cout << el[i] << ", ";
    }

    cout << el[brEl - 1] << " )" << endl;
}

template <class T, int D>
bool Niz<T, D>::insertNiz(const T& t) {
    if(brEl < D) {          /// proverava da li je brojac elemenata veci od DOZVOLJENOG broja!
        el[brEl] = t;
        brEl++;
        return true;
    } else return false;
}

/// prijateljske funkcije.


template <class T, int D>
/// ove objekte saljem po referenci kako ne bih napravio njihovu kopiju
/// unutar ove funkcije, nego saljem originalne.
bool operator==(const Niz<T, D> &rn1, const Niz<T, D> &rn2){
    bool jednaki = false;
    if(rn1.getBrEl() == rn2.getBrEl()) {
        for(int i = 0; i < rn1.getBrEl(); i++) {
            if (rn1[i] == rn2[i]){
                jednaki = true;
            } else {
                jednaki = false;
            }
        }
    } else {
        jednaki = false;;
    }

    return jednaki;
}

template <class T, int D>
bool operator!=(const Niz<T, D> &rn1, const Niz<T, D> &rn2){
    return !(rn1 == rn2);
}


#endif // NIZ_HPP_INCLUDED



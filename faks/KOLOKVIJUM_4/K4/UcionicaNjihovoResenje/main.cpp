#include "ucionica.hpp"

int main()
{
    cout << endl << endl << "TESTIRANJE KLASE OSOBA" << endl << endl;

    Osoba o;
    o.predstaviSe();

    o.setIme("Pera");
    o.setPrezime("Petrovic");
    o.predstaviSe();

    Osoba o2(o);
    o2.predstaviSe();



    cout << endl << endl << "TESTIRANJE KLASE STUDENT" << endl << endl;

    Student s;
    s.predstaviSe();

    s.setIme("Marko");
    s.setPrezime("Markovic");
    s.setBrojIndeksa(12345);
    s.predstaviSe();

    Student s2(s);
    s2.predstaviSe();



    cout << endl << endl << "TESTIRANJE KLASE STUDENT" << endl << endl;

    Profesor p;
    p.predstaviSe();

    p.setIme("Ivana");
    p.setPrezime("Ivanovic");
    p.setNazivPredmeta("Objektno programiranje");
    p.predstaviSe();

    Profesor p2(p);
    p2.predstaviSe();



    cout << endl << endl << "TESTIRANJE KLASE UCIONICA" << endl << endl;

    Ucionica u(5);
    u.setNaziv("205a");
    DinString nazivU = u.getNaziv();
    cout << "==========" << nazivU << "============";
    cout << "==========" << u.getNaziv() << "============";

    u.ispis();

    if(u.dodaj(p)) {
        cout << "Uspesno dodavanje." << endl;
    } else {
        cout << "Neuspesno dodavanje." << endl;
    }

    if(u.dodaj(p2)) {
        cout << "Uspesno dodavanje." << endl;
    } else {
        cout << "Neuspesno dodavanje." << endl;
    }

    if(u.dodaj(s)) {
        cout << "Uspesno dodavanje." << endl;
    } else {
        cout << "Neuspesno dodavanje." << endl;
    }

    if(u.dodaj(s2)) {
        cout << "Uspesno dodavanje." << endl;
    } else {
        cout << "Neuspesno dodavanje." << endl;
    }

    if(u.dodaj(o)) {
        cout << "Uspesno dodavanje." << endl;
    } else {
        cout << "Neuspesno dodavanje." << endl;
    }

    if(u.dodaj(o2)) {
        cout << "Uspesno dodavanje." << endl;
    } else {
        cout << "Neuspesno dodavanje." << endl;
    }

    u.ispis();

    return 0;
}

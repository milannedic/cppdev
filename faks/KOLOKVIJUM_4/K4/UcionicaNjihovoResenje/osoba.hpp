#ifndef OSOBA_DEF
#define OSOBA_DEF

#include "dinstring.hpp"

class Osoba {
	protected:
		DinString ime, prezime;

	public:
		Osoba() : ime("niko"), prezime("niko") {}

		Osoba(const Osoba &ro) : ime(ro.ime), prezime(ro.prezime) {}

        DinString getIme() const {
            return ime;
        }

        void setIme(const DinString& i) {
            ime = i;
        }

        DinString getPrezime() const {
            return prezime;
        }

        void setPrezime(const DinString& p) {
            prezime = p;
        }

		virtual void predstaviSe() const {
            cout << "Zovem se " << ime << " " << prezime << "." << endl;
		}
};

#endif

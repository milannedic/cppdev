#ifndef NIZ_HPP_INCLUDED
#define NIZ_HPP_INCLUDED

template <class T, int D>
class Niz {
    private:
        T el[D];
        int brEl;
    public:

    Niz() { brEl = 0; }
    ~Niz(){}

    int getBrEl() const {
        return brEl;
    }


    T& operator[](int i) {      ///  operator dodjele, vraca referencu
        return el[i];
    }

    T operator[](int i) const {
        return el[i];
    }


    Niz<T, D> & operator=(const Niz<T,D> &);
    void printNiz() const;
    bool insertNiz(const T&);

};

/// PREKLOPANJE OPERATORA =
template <class T, int D>
Niz<T, D>& Niz<T,D>::operator=(const Niz<T, D> &op){

}

/// PREKLOPANJE OPERATORA ==
template <class T, int D>
bool operator==(const Niz<T,D> &op1, const Niz<T,D> &op2){
    bool jednaki = false;

    if(op1.gtBrEl() == op2.gtBrEl()) {
        for(int i = 0; i < op1.gtBrEl(); i++){
            if(op1[i] != op2[i]){
                jednaki = false;
            } else {
                jednaki = true;
            }
        }
    } else {
        jednaki = false;
    }

    return jednaki;
}

template <class T, int D>
bool operator!=(const Niz<T,D> &op1, const Niz<T,D> &op2){
    return !(op1 == op2);
}

#endif // NIZ_HPP_INCLUDED

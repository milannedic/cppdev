#ifndef KOLOKVIJUM_HPP_INCLUDED
#define KOLOKVIJUM_HPP_INCLUDED

#include <cstdlib>
#include "Test.hpp"
class Kolokvijum: public Test{
    private:

    public:
        // Kolokvijum(char *naz = "") : naziv(naz), osvojeniPoeni(rand()%25 + 1) {}
        Kolokvijum(const char *naz) : Test(naz, rand() % 25 + 1) {}
        Kolokvijum() : Test("", 0) {}

        int getPoeni(){
            return osvojeniPoeni;
        }

        bool polozio() {
            return osvojeniPoeni > 12 ? true : false;
        }


};

#endif // KOLOKVIJUM_HPP_INCLUDED

#include <iostream>
#include "Student.hpp"

using namespace std;



int main()
{
    cout << endl << endl << "TESTIRANJE KLASE KOLOKVIJUM" << endl << endl;

    Kolokvijum k, k1("K1"), k2("K2"), k3("K3"), k4("K4");

    k.print();
    cout << "K : Poeni = " << k.getPoeni() << " Polozio = " << (k.polozio() ? "jeste" : "nije") << endl;

    k1.print();
    cout << "K1 : Poeni = " << k1.getPoeni() << " Polozio = " << (k1.polozio() ? "jeste" : "nije") << endl;

    k2.print();
    cout << "K2 : Poeni = " << k2.getPoeni() << " Polozio = " << (k2.polozio() ? "jeste" : "nije") << endl;

    k3.print();
    cout << "K3 : Poeni = " << k3.getPoeni() << " Polozio = " << (k3.polozio() ? "jeste" : "nije") << endl;
    return 0;


    cout << endl << endl << "TESTIRANJE KLASE PREDMET" << endl << endl;

    Predmet p, p1("TEK"), p2("SIS");


    p.ispis();
    p1.ispis();
    p2.ispis();

    p1.dodajTest(k1);
    p1.ispis();


}

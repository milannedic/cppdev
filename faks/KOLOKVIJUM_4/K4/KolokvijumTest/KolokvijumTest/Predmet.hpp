#ifndef PREDMET_HPP_INCLUDED
#define PREDMET_HPP_INCLUDED

#include "Kolokvijum.hpp"
#include "list.hpp"

class Predmet {
    private:
        DinString naziv;
        List <Test*> testovi;
    public:

    void dodajTest(Test& t) {
        testovi.add(testovi.size() + 1, &t);       // ide &t zato sto lista sadrzi pokazivace na OBJEKTE
    }

    DinString getNaziv() const {
        return naziv;
    }

    Predmet(const char *naz = "") : naziv(naz) {}
    Predmet(const Predmet &pr) : naziv(pr.naziv), testovi(pr.testovi) {

    }

    bool polozio(){
        Test *tRet;
        for(int i = 1; i <= testovi.size(); i++){
            testovi.read(i, tRet);
            if(!tRet->polozio()){
                return false;
            }
        }

        return getBodovi() > 55 ? true : false;
    }

    int getBodovi(){
        Test *t;
        int sumaBodova = 0;

        for(int i = 0; i < testovi.size() + 1; i++){
            testovi.read(i, t);
            sumaBodova += t->getPoeni();
        }

        return sumaBodova;
    }

    void ispis() const {
        cout << "====================================" << endl;
        cout << "Naziv testa: " << naziv << endl;
        if(testovi.size() == 0) {
            cout << "Nema testova!" << naziv << endl;
        } else {
            Test *t;
            cout << "Spisak testova: " << endl << endl;
            for(int i = 0 ; i < testovi.size(); i++){
                testovi.read(i, t);
                t->print();
            }
        }
        cout << "====================================" << endl;
    }
};

#endif // PREDMET_HPP_INCLUDED

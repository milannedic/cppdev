#ifndef TEST_HPP_INCLUDED
#define TEST_HPP_INCLUDED

#include "dinstring.hpp"

class Test {
    protected:                  /// OVO NIJE NAGLASENO DA JE PROTECTED, BLEJACI!
        DinString naziv;
        int osvojeniPoeni;
    public:

    virtual int getPoeni() = 0;
    virtual bool polozio() = 0;


    Test(const char *naz = "", int os = 0) : naziv(naz), osvojeniPoeni(os) {}

    void print(){
        cout << " \t Naziv testa je : " << naziv << endl;
        cout << " \t Osvojeni poeni : " << osvojeniPoeni << endl;
    }

};

#endif // TEST_HPP_INCLUDED


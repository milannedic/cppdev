#ifndef STUDENT_HPP_INCLUDED
#define STUDENT_HPP_INCLUDED

#include "Predmet.hpp"
//Napisati klasu Student, koji u sebi ima dva predmeta. Klasa treba da ima konstruktor sa parametrima
//koji prima dva predmeta.

// Napisati i metodu void ispis() koja treba da ispiše informacije o svakom
//testu koji je student uradio iz oba predmeta, kao i naziv predmeta iz kog je osvojio više poena na
//testovima.
class Student {
    private:
        Predmet p1;
        Predmet p2;
    public:

        Student(const Predmet &predmet1, const Predmet &predmet2) : p1(predmet1), p2(predmet2) {
            // bleya
        }

        void ispis() {
            p1.ispis();
            p2.ispis();
            cout << "Student je osvojio vise poena iz predmeta: " << (p1.getBodovi() > p2.getBodovi() ? p1.getNaziv() : p2.getNaziv()) << endl;
        }


};

#endif // STUDENT_HPP_INCLUDED

#ifndef LEK_HPP_INCLUDED
#define LEK_HPP_INCLUDED

#include "dinstring.hpp"
#include "Artikal.hpp"
///
///    Iz apstraktne klase Artikal izvesti klasu Lek, koja sadrži objekte-članove:
///    jkl (long), naziv (DinString), jedinicnaCena (double) i kolicina (int).

///    Klasa
///    treba da sadrži konstruktor bez parametara, konstruktor sa parametrima
///    (gde je parametar za naziv tipa char[]), konstruktor sa parametrima (gde je
///    parametar za naziv tipa DinString&), konstruktor kopije, get/set metode,
///    realizacije odgovarajućih apstraktnih metoda i metodu za ispis.
class Lek: public Artikal {
    private:
        long jkl;
        DinString naziv;
        double jedinicnaCena;
        int kolicina;
    public:

        Lek() : jkl(0), naziv(""), jedinicnaCena(0), kolicina(0) {}
        Lek(long jkl_p, const char n[], double cena, int kol) : jkl(jkl_p), naziv(n), jedinicnaCena(cena), kolicina(kol) {}
        Lek(long jkl_p, const DinString &n, double cena, int kol) : jkl(jkl_p), naziv(n), jedinicnaCena(cena), kolicina(kol) {}
        Lek(const Lek &l) : jkl(l.jkl), naziv(l.naziv), jedinicnaCena(l.jedinicnaCena), kolicina(l.kolicina) {}



        double racunajCenu() {
            return kolicina * jedinicnaCena;
        }

        void lekIspis() {
            cout << endl << "-------------------------" << endl;
            cout << "JKL :" << jkl << endl;
            cout << "Naziv leka :" << naziv << endl;
            cout << "Jedinicna cena :" << jedinicnaCena << endl;
            cout << "Kolicina :" << kolicina << endl;
        }

        /// Getteri
        /// Getteri
        /// Getteri
        long getJkl() const {
            return jkl;
        }

        DinString getNaziv() const {
            return naziv;
        }

        double getJedinicnaCena() const {
            return jedinicnaCena;
        }

        int getKolicina() const {
            return kolicina;
        }

        /// Setteri
        /// Setteri
        /// Setteri
        void setJkl(long jkl_p)  {
            jkl = jkl_p;
        }

        void setNaziv(const DinString &naz) {
            naziv = naz;
        }

        void setJedinicnCena( double cena) {
            jedinicnaCena = cena ;
        }

        void setKolicina(int kol) {
            kolicina = kol;
        }

};
#endif // LEK_HPP_INCLUDED

#ifndef EVIDENCIJALEKOVA_HPP_INCLUDED
#define EVIDENCIJALEKOVA_HPP_INCLUDED

#include <iostream>

#include "dinstring.hpp"
#include "Lek.hpp"
#include "list.hpp"
//    Napisati klasu EvidencijaLekova, koja sadrži objekte-članove:
//    nazivApoteke (DinString), datum (DinString) i evidencija (List<Lek>).

//    Klasa treba da sadrži konstruktor bez parametara, get/set metode (osim za
//    polje evidencija),

///   metodu za unos novog leka u evidenciju (pomoću
//    tastature, dodati ga na kraj evidencije), metodu za dodavanje leka u
//    evidenciju (pomoću već postojećeg objekta klase Lek,dodati ga na početak
//    evidencije), metodu za brisanje leka iz evidencije, metodu za sortiranje
//    evidencije po jediničnoj ceni leka (od najjeftinijeg leka do najskupljeg), kao i
//    metodu za ispis.
class EvidencijaLekova {
    private:
        DinString nazivApoteke;
        DinString datum;
        List<Lek> evidencija;
    public:
        EvidencijaLekova() : nazivApoteke("Gradska apoteka"), datum("01.01.2016.") {}

        /// Setteri
        /// Setteri
        /// Setteri
        void setNazivApoteke(const DinString &naziv){
            nazivApoteke = naziv;
        }

        void setDatum(const DinString &d){
            datum = d;
        }

        /// Getteri
        /// Getteri
        /// Getteri
        DinString getNazivApoteke() const {
            return nazivApoteke;
        }

        DinString getDatum() const {
            return datum;
        }

        /// =============================================
        bool unosLeka() {
            bool ret = false;
//            long jkl;
//            DinString naziv;
//            double jedinicnaCena;
//            int kolicina;
            long jklP = 0;
            char pNaziv[100];
            double cena;
            int kol;

            ///
            cout << "Unesite JKL: " << endl;
            cin >> jklP;

            cout << endl;

            ///
            cout << "Unesite naziv leka: " << endl;
            cin >> pNaziv;

            cout << endl;

            ///
            cout << "Unesite jedinicnu cenu: " << endl;
            cin >> cena;

            cout << endl;

            ///
            cout << "Unesite kolicinu: " << endl;
            cin >> kol;

            cout << endl;

            /// Lek(long jkl_p, const char n[], double cena, int kol) : jkl(jkl_p), naziv(n), jedinicnaCena(cena), kolicina(kol) {}
            Lek l(jklP, pNaziv, cena, kol);

            return evidencija.add(evidencija.size() + 1, l);

        }

        bool ukloniLek(int position) {
            return evidencija.remove(position);
        }

        bool dodajPostojeciLek(const Lek& l) {
            bool ret = false;

            ret = evidencija.add(1, l);

            return ret;
        }

        void ispisiEvidenciju() {
            cout << " ====================================== " << endl;
            cout << "Naziv apoteke : " << getNazivApoteke() << "; datum : " << getDatum() << " ::::" << endl;

            if(evidencija.empty()) {
                cout << "Nema lekova u evidenciji!" << endl;
            } else {
                Lek l;
                for(int i = 1; i <= evidencija.size(); i++) {
                    evidencija.read(i, l);
                    l.lekIspis();
                }
            }
        }


        void sortirajLowToHigh() {
            Lek l1;
            Lek l2;

            for(int i = 1; i <= evidencija.size(); i++){
                for(int j = i + 1; j <= evidencija.size(); j++){
                    evidencija.read(i, l1);
                    evidencija.read(j, l2);

                    if(l1.getJedinicnaCena() > l2.getJedinicnaCena()) {
                        evidencija.remove(i);
                        evidencija.add(i, l2);
                        evidencija.remove(j);
                        evidencija.add(j, l1);
                    }
                }
            }
        }


};

#endif // EVIDENCIJALEKOVA_HPP_INCLUDED

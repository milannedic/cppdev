#include <iostream>

#include "Lek.hpp"
#include "EvidencijaLekova.hpp"
using namespace std;

int main()
{
    /// Lek l(jklP, pNaziv, cena, kol);

    Lek l;
    Lek l10(1234567, "Aspirin C", 100, 13);
    Lek l1(1234567, "Aspirin C", 1200, 13);
    Lek l2(12347, "Nesto", 400, 13);
    Lek l3(l10);

    l.lekIspis();
    l1.lekIspis();
    l2.lekIspis();
    l3.lekIspis();

    cout << "TESTIRANJE APOTEKE" << endl << endl;

    EvidencijaLekova apoteka;
    //apoteka.unosLeka();
    ///
    if(apoteka.dodajPostojeciLek(l1)) {
        cout << endl << "Uspesno dodat lek!" << endl;
    } else {
        cout << endl << "NIje dodato!" << endl;
    }

    ///
    if(apoteka.dodajPostojeciLek(l2)) {
        cout << endl << "Uspesno dodat lek!" << endl;
    } else {
        cout << endl << "NIje dodato!" << endl;
    }

    ///
    if(apoteka.dodajPostojeciLek(l3)) {
        cout << endl << "Uspesno dodat lek!" << endl;
    } else {
        cout << endl << "NIje dodato!" << endl;
    }

/*
    /// Unos sa tastature...
    if(apoteka.unosLeka()) {
        cout << endl << "Uspesno dodat lek sa tastature!" << endl;
    } else {
        cout << endl << "Nije dodato!" << endl;
    }
*/

    Lek l5(9877787, "Epinifrin", 400, 13);
    ///
    if(apoteka.dodajPostojeciLek(l5)) {
        cout << endl << "Uspesno dodat lek!" << endl;
    } else {
        cout << endl << "NIje dodato!" << endl;
    }


    apoteka.ispisiEvidenciju();

    //apoteka.ukloniLek(3);
    //cout << endl << "Uklanjam lek!" << endl;

    //apoteka.ispisiEvidenciju();

    apoteka.sortirajLowToHigh();
    cout << endl << "=============== Ispis nakon sortiranja ===============" << endl;


    apoteka.ispisiEvidenciju();

    return 0;
}

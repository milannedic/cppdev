#ifndef UCIONICA_HPP_INCLUDED
#define UCIONICA_HPP_INCLUDED

#include "Student.hpp"
#include "Profesor.hpp"
#include "list.hpp"
//    Napisati klasu Učionica koja ima listu osoba koji su u njoj prisutne. Učionica ima svoj naziv i maksimalan
//    broj osoba koje mogu da stanu u nju. Maksimalan broj prisutnih osoba se zadaje prilikom konstrukcije
//    objekta klase Učionica. Prilikom dodavanja nove osobe proveriti da li u učionici ima slobodnih mesta.

//    Učionica:
//    get/set naziva učionice
//    metoda za dodavanje nove osobe u učionicu
//    ispis svih prisutnih osoba u učionici
//    konstruktor sa parametrima (za kapacitet učionice)
class Ucionica {
    private:
        List <Osoba*> osobe;
        DinString naziv;
        int kapacitet;
    public:

    Ucionica(int kap) : kapacitet(kap){}

    void setNazivUcionice(const DinString &uc){
        naziv = uc;
    }

    DinString getNazivUcionice(){
        return naziv;
    }

    bool dodajOsobu(Osoba &os){
        if(osobe.size() < kapacitet) {
            return osobe.add(osobe.size() + 1, &os);
        }

        return false;
    }

    void ispisUcionica() {
        Osoba *os;

        if(osobe.size() == 0) {
            cout << "Ucionica je prazna!" << endl;
        } else {
            cout << "Spisak osoba u ucionici:" << endl;
            cout << "---------------------------------" << endl;

            for(int i = 1; i <= osobe.size(); i++){
                osobe.read(i, os);
                cout << "Osoba " << i << " : " << endl;
                os->ispisiPodatke();
                cout << "---------------------------------" << endl;
            }
        }
    }

};

#endif // UCIONICA_HPP_INCLUDED

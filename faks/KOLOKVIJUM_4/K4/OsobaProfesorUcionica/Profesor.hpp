#ifndef PROFESOR_HPP_INCLUDED
#define PROFESOR_HPP_INCLUDED

#include "Osoba.hpp"
//    Profesor:
//    get/set naziv predmeta
//    preklopljena metoda za ispis
//    konstruktor bez parametara
//    konstruktor kopije
class Profesor : public Osoba {
    private:
        DinString predmet;
    public:

    void ispisiPodatke() {
        cout << "Ime profesora : " << ime << endl;
        cout << "Prezime profesora : " << prezime << endl;
        cout << "Predmet koji predaje : " << predmet << endl;
    }

    Profesor() : predmet("") {}
    Profesor(const Profesor &prof) : predmet(prof.predmet), Osoba((Osoba)prof) {}

    void setPredmet(const char *pr = ""){
        predmet = pr;
    }

    DinString getPredmet(){
        return predmet;
    }

};

#endif // PROFESOR_HPP_INCLUDED

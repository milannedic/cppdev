#include <iostream>

#include "Ucionica.hpp"
#include "Student.hpp"
#include "Profesor.hpp"
#include "dinstring.hpp"

using namespace std;

int main()
{
    Ucionica u(5); // kapacitet = 60

    //DinString a("205A");

    u.setNazivUcionice("206a");


    DinString nazivUcionice = u.getNazivUcionice();
    cout << "Naziv ucionice je : " << nazivUcionice << endl;
    cout << "Naziv ucionice je : " << u.getNazivUcionice() << endl;

    Osoba o, o1, o2;

    o.setIme("Milan");
    o.setPrezime("Nedic");
    o.ispisiPodatke();

    o1.setIme("Lemi");
    o1.setPrezime("Tebra");
    o1.ispisiPodatke();

    o2.setIme("Dzon");
    o2.setPrezime("Bodemseuvejn");
    o2.ispisiPodatke();
    cout << "-------------" << endl;

    Osoba o3(o2);
    o3.ispisiPodatke();


    cout << endl << endl << "TESTIRANJE KLASE STUDENT" << endl << endl;

    Student s;
    s.ispisiPodatke();

    s.setIme("Marko");
    s.setPrezime("Markovic");
    s.setBrIndeksa(123456786);
    s.ispisiPodatke();

    Student s2(s);
    s2.ispisiPodatke();

    cout << endl << endl << "TESTIRANJE KLASE PROFESOR" << endl << endl;

    Profesor p;
    p.ispisiPodatke();

    p.setIme("Ivana");
    p.setPrezime("Ivanovic");
    p.setPredmet("Objektno programiranje");
    p.ispisiPodatke();

    Profesor p2(p);
    p2.ispisiPodatke();


    cout << endl << endl << "TESTIRANJE KLASE UCIONICA" << endl << endl;

    DinString nazivU = u.getNazivUcionice();
    cout << "==========" << nazivU << "============";
    cout << "==========" << u.getNazivUcionice() << "============";


    u.ispisUcionica();
    if(u.dodajOsobu(p)) {
        cout << "Uspesno dodavanje." << endl;
    } else {
        cout << "Neuspesno dodavanje." << endl;
    }

    if(u.dodajOsobu(p2)) {
        cout << "Uspesno dodavanje." << endl;
    } else {
        cout << "Neuspesno dodavanje." << endl;
    }

    if(u.dodajOsobu(s)) {
        cout << "Uspesno dodavanje." << endl;
    } else {
        cout << "Neuspesno dodavanje." << endl;
    }

    if(u.dodajOsobu(s2)) {
        cout << "Uspesno dodavanje." << endl;
    } else {
        cout << "Neuspesno dodavanje." << endl;
    }

    if(u.dodajOsobu(o)) {
        cout << "Uspesno dodavanje." << endl;
    } else {
        cout << "Neuspesno dodavanje." << endl;
    }

    if(u.dodajOsobu(o2)) {
        cout << "Uspesno dodavanje." << endl;
    } else {
        cout << "Neuspesno dodavanje." << endl;
    }

    u.ispisUcionica();


    return 0;

}

#ifndef STUDENT_HPP_INCLUDED
#define STUDENT_HPP_INCLUDED

#include "Osoba.hpp"
//    Student:
//    get/set broj indeksa
//    preklopljena metoda za ispis
//    konstruktor bez parametara
//    konstruktor kopije
class Student: public Osoba {
    private:
        int brojIndeksa;
    public:

    void ispisiPodatke() {
        cout << "Ime studenta : " << ime << endl;
        cout << "Prezime studenta : " << prezime << endl;
        cout << "Broj indeksa : " << brojIndeksa << endl;
    }

    Student() : brojIndeksa(0) {}
    Student(const Student &s) : brojIndeksa(s.brojIndeksa), Osoba((Osoba)s) {}


    void setBrIndeksa(int indeks){
        brojIndeksa = indeks;
    }

    int getBrIndeksa(){
        return brojIndeksa;
    }


};

#endif // STUDENT_HPP_INCLUDED

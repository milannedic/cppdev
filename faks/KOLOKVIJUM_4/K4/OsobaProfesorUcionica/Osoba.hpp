#ifndef OSOBA_HPP_INCLUDED
#define OSOBA_HPP_INCLUDED

//Napisati klasu Osoba ima polja ime, prezime, i ima virtuelnu metodu za ispis podataka o osobi. Iz klase
//Osoba izvesti klasu Student. Student ima dodatno broj indeksa. Takođe, iz klase Osoba izvesti i klasu
//Profesor. Profesor ima dodatno i naziv predmeta koji predaje. Klase Student i Profesor imaju
//preklopljenu metodu za ispis podataka o sebi.

#include "dinstring.hpp"

//    Osoba:
//    get/set Ime
//    get/set Prezime
//    virtuelna metoda za ispis
//    konstruktor bez parametara
//    konstruktor kopije
class Osoba {
    protected:
        DinString ime;
        DinString prezime;
    public:

        virtual void ispisiPodatke() {
            cout << "Ime osobe : " << ime << endl;
            cout << "Prezime osobe : " << prezime << endl;
        }

        /// Konstruktori
        Osoba() : ime(""), prezime("") {}
        Osoba(const Osoba &os) : ime(os.ime), prezime(os.prezime) {}


        void setIme(const char* name = ""){
            ime = name;
        }

        void setPrezime(const char* prez = ""){
            prezime = prez;
        }

        DinString getIme(){
            return ime;
        }

        DinString getPrezime(){
            return prezime;
        }
};

#endif // OSOBA_HPP_INCLUDED

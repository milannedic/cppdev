#include <iostream>
#include "Trezor.hpp"
#include "Dijamant.hpp"

using namespace std;

int main()
{
    Trezor<Dijamant, 100> trezor;
    Dijamant d1, d2(100000, 50), d3(d2);

    cout << "D1: Vrednost: " << d1.getVrednost() << " a karat : " << d1.getKarat() << endl;
    cout << "D2: Vrednost: " << d2.getVrednost() << " a karat : " << d2.getKarat() << endl;
    cout << "D3: Vrednost: " << d3.getVrednost() << " a karat : " << d3.getKarat() << endl;

    cout << "ubaciSadrzaj(d1) ==> " << trezor.ubaciSadrzaj(d1) << endl;
    cout << "Broj popunjenih sefova: " << trezor.getBrojPopunjenihSefova() << endl;
    cout << "ubaciSadrzaj(d2) ==> " << trezor.ubaciSadrzaj(d2) << endl;
    cout << "Broj popunjenih sefova: " << trezor.getBrojPopunjenihSefova() << endl;
    cout << "ubaciSadrzaj(d3) ==> " << trezor.ubaciSadrzaj(d3) << endl;
    cout << "Broj popunjenih sefova: " << trezor.getBrojPopunjenihSefova() << endl;


    if(trezor.izbaciSadrzaj(2)) {
        cout << "Broj popunjenih sefova: " << trezor.getBrojPopunjenihSefova() << endl;
    }

    if(trezor.izbaciSadrzaj(1)) {
        cout << "Broj popunjenih sefova: " << trezor.getBrojPopunjenihSefova() << endl;
    }

    cout << "Trezor: izbaci 50 rezultat=" << trezor.izbaciSadrzaj(50) << endl;
    cout << "Trezor: broj popunjenih=" << trezor.getBrojPopunjenihSefova() << endl;

    return 0;
}

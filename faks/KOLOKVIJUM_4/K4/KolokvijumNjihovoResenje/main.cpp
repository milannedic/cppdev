#include "student.hpp"

int main()
{
    cout << endl << endl << "TESTIRANJE KLASE KOLOKVIJUM" << endl << endl;

    Kolokvijum k, k1("K1"), k2("K2"), k3("K3"), k4("K4");

    k.print();
    cout << "K: poeni = " << k.getPoeni() << ", polozio = " << (k.polozio() ? "jeste" : "nije") << endl << endl;

    k1.print();
    cout << "K1: poeni = " << k1.getPoeni() << ", polozio = " << (k1.polozio() ? "jeste" : "nije") << endl << endl;

    k2.print();
    cout << "K2: poeni = " << k2.getPoeni() << ", polozio = " << (k2.polozio() ? "jeste" : "nije") << endl << endl;

    k3.print();
    cout << "K3: poeni = " << k3.getPoeni() << ", polozio = " << (k3.polozio() ? "jeste" : "nije") << endl << endl;

    k4.print();
    cout << "K2: poeni = " << k4.getPoeni() << ", polozio = " << (k4.polozio() ? "jeste" : "nije") << endl << endl;


    cout << endl << endl << "TESTIRANJE KLASE PREDMET" << endl << endl;

    Predmet p, p2("OP"), p3("MISS");

    p.ispis();
    p2.ispis();
    p3.ispis();

    p2.dodajTest(k1);
    p2.ispis();

    p2.dodajTest(k2);
    p2.dodajTest(k3);
    p2.dodajTest(k4);
    p2.ispis();

    cout << "Ukupno bodova osvojeno: " << p2.getBodovi() << endl;
    cout << (p2.polozio() ? "Polozio OP" : "Nije polozio OP") << endl;

    Predmet p4(p2);

    p4.ispis();


    cout << endl << endl << "TESTIRANJE KLASE STUDENT" << endl << endl;

    Student s(p2, p3);

    s.ispis();


    return 0;
}

#ifndef ARTIKAL_HPP_INCLUDED
#define ARTIKAL_HPP_INCLUDED

#include <iostream>
using namespace std;

class Artikal{
    public:
        virtual double izracunajCenu() const = 0;
};

#endif // ARTIKAL_HPP_INCLUDED

#include "evidencijaLekova.hpp"

int main()
{
    cout << "TESTIRANJE KLASE LEK" << endl << endl;

    DinString d("Pressing");
    Lek l1, l2(2, "Aspirin", 22.33, 10), l3(3, d, 34.56, 20), l4(l3);

    l1.ispis();
    l2.ispis();
    l3.ispis();
    l4.ispis();


    cout << endl << endl << "TESTIRANJE KLASE EVIDENCIJA LEKOVA" << endl;

    EvidencijaLekova e;

    e.ispis();


    if(e.dodajLek()){
        cout << "Lek uspesno dodat!" << endl;
    } else {
        cout << "Lek neuspesno dodat!" << endl;
    }

    e.ispis();

    if(e.dodajLek()){
        cout << "Lek uspesno dodat!" << endl;
    } else {
        cout << "Lek neuspesno dodat!" << endl;
    }

    e.ispis();


    if(e.dodajPostojeciLek(l1)){
        cout << "Lek uspesno dodat!" << endl;
    } else {
        cout << "Lek neuspesno dodat!" << endl;
    }

    e.ispis();


    e.sortLekovaPoJedinicnojCeni();

    cout << "Lekovi sortirani po jedinicnoj ceni, od najjeftinijeg do najskupljeg!" << endl;

    e.ispis();


    if(e.ukloniLek(1)){
        cout << "Najjeftiniji lek (po jedinicnoj ceni) uspesno uklonjen!" << endl;
    } else {
        cout << "Lek neuspesno uklonjen!" << endl;
    }

    e.ispis();

    return 0;
}

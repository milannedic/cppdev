#include <iostream>
#include "DynString.hpp"

using namespace std;
int main(){

    DynString ds1;
    DynString ds2("Ja sam Milan");
    DynString ds3(ds2);
    DynString ds4("Nedic");
    DynString ds5("Milan Nedic EE131/2015");


    cout << "Prazan string ds1 =            " << ds1 << endl;
    cout << "String ds2 =                   " << ds2 << endl;
    cout << "Konstruktor kopije: ds3(ds2) = " << ds3 << endl;

    cout << "Duzina ds2                   = " << ds2.length() << endl;

    ds2 = ds4;
    cout << "ds2 nakon ds2 = ds4          = " << ds2 << endl;

    ds2 = ds5;
    cout << "ds2 nakon ds2 = ds5          = " << ds2 << endl;

    ds2 = ds5 = ds4;
    cout << "ds2 nakon ds2 = ds5 = ds4    = " << ds2 << endl;

    ds2[4] = 'Q';
    cout << "ds2 nakon ds2[4] = \'Q\'       = " << ds2 << endl;

    char test = ds2[3];
    cout << "test = ds2[3]           test = " << test << endl;

    test = ds2[4];
    cout << "test = ds2[4]           test = " << test << endl;

    cout << "================ Operator== =================" << endl;
    /// vracam na staro
    ds2[4] = 'c';
    DynString ds6(ds2);
    cout << "Trenutno stanje ds2: " << ds2 << endl;
    cout << "Trenutno stanje ds3: " << ds3 << endl;
    cout << "Trenutno stanje ds4: " << ds4 << endl;
    cout << "Trenutno stanje ds6: " << ds6 << endl;


    cout << "ds2 == ds3 ? ====>" << ((ds2 == ds3) ? "Da" : "Ne") << endl;
    cout << "ds2 == ds4 ? ====>" << ((ds2 == ds4) ? "Da" : "Ne") << endl;
    cout << "ds2 == ds6 ? ====>" << ((ds2 == ds6) ? "Da" : "Ne") << endl;

    cout << "================ Operator!= =================" << endl;
    /// vracam na staro
    DynString ds7(ds3);
    cout << "Trenutno stanje ds2: " << ds2 << endl;
    cout << "Trenutno stanje ds3: " << ds3 << endl;
    cout << "Trenutno stanje ds4: " << ds4 << endl;
    cout << "Trenutno stanje ds6: " << ds7 << endl;


    cout << "ds2 != ds3 ? ====>" << ((ds2 != ds3) ? "Da" : "Ne") << endl;
    cout << "ds2 != ds4 ? ====>" << ((ds2 != ds4) ? "Da" : "Ne") << endl;
    cout << "ds2 != ds7 ? ====>" << ((ds2 != ds7) ? "Da" : "Ne") << endl;

    cout << "================ Operator+ =================" << endl;
    cout << "Trenutno stanje ds2: " << ds2 << endl;
    cout << "Trenutno stanje ds3: " << ds3 << endl;
    cout << "Trenutno stanje ds4: " << ds4 << endl;
    cout << "Trenutno stanje ds7: " << ds7 << endl;

    cout << "ds2 + ds3               == " << (ds2 + ds3) << endl;
    cout << "ds2 + ds4 + ds7         == " << (ds2 + ds4 + ds7)  << endl;

    cout << "========= KON-KA-TE-NAA-CI-JAAAA ==========" << endl;
    cout << "Trenutno stanje ds2: " << ds2 << endl;
    cout << "Trenutno stanje ds3: " << ds3 << endl;
    cout << "Trenutno stanje ds4: " << ds4 << endl;
    cout << "Trenutno stanje ds7: " << ds7 << endl;

    DynString ds10("testni 2345 string");
    DynString ds8 = ds2 += ds10;
    DynString ds9 = ds10 += ds4 += ds7;
    cout << "ds2 += ds10               == " << ds8 << endl;
    cout << "ds10 += ds4 += ds7        == " << ds9 << endl;

    return 0;
}

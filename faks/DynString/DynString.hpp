#ifndef DINSTRING_DEF
#define DINSTRING_DEF

#include <iostream>

using namespace std;
class DynString {

    private:
        int duzina;
        char *text;
    public:
        /// DESTRUKTOR
        ~DynString();

        /// KONSTRUKTORI
        DynString();                    // bez parametara
        DynString(const char textIn[]);   // sa parametrima
        DynString(const DynString &);   // konsturktor kopije

        int length() const;

        /// vraca po referenci da bi mogao lancano odraditi;
        /// ds1 = ds2 = ds3;
        DynString& operator=(const DynString &);
        DynString& operator+=(const DynString &);

        char& operator[](int i);
        char operator[](int i) const;

        friend bool operator==(const DynString &, const DynString &);
        friend bool operator!=(const DynString &, const DynString &);

        friend DynString operator+(const DynString &, const DynString &);

        friend ostream& operator<<(ostream& out, const DynString &ds);


};

#endif


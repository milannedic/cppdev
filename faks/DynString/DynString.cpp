#include "DynString.hpp"
#include <iostream>

using namespace std;

DynString::~DynString(){
    delete [] text;
}

// KONSTRUKTOR KOPIJE
DynString::DynString(const DynString &ds){
    duzina = ds.duzina;

    text = new char[duzina + 1];
    for(int i = 0; i < duzina; i++){
        text[i] = ds.text[i];
    }

    text[duzina] = '\0';

}


// KONSTRUKTOR BEZ PARAMETARA
DynString::DynString(){
        duzina = 0;
        text = NULL;
}

// KONSTRUKTOR SA PARAMETRIMA
DynString::DynString(const char textIn[]){
        duzina = 0;

        // ocitam samo duzinu stringa...
        while(textIn[duzina] != '\0'){
            duzina++;
        }

        text = new char[duzina + 1];
        for(int i = 0; i < duzina; i++){
            text[i] = textIn[i];
        }

        text[duzina] = '\0';
}

int DynString::length() const{
    return duzina;
}

/// PREKLAPANJE operator= RADI SA REFERENCOM A I BEZ NJE!!!
/// PREKLAPANJE operator= RADI SA REFERENCOM A I BEZ NJE!!!
/// PREKLAPANJE operator= RADI SA REFERENCOM A I BEZ NJE!!!
/// PREKLAPANJE operator= RADI SA REFERENCOM A I BEZ NJE!!!
/// PREKLAPANJE operator= RADI SA REFERENCOM A I BEZ NJE!!!
/// PREKLAPANJE operator= RADI SA REFERENCOM A I BEZ NJE!!!
/// PREKLAPANJE operator= RADI SA REFERENCOM A I BEZ NJE!!!
DynString& DynString::operator=(const DynString &ds) {
    if(this != &ds){
        delete [] text;
        duzina = ds.duzina;
        text = new char[duzina];

        for(int i = 0; i < ds.duzina; i++){
            text[i] = ds.text[i];
        }

        text[duzina] = '\0';
    }

    return *this;

}

// SUBSCRIPT indeksiranje, OPERATOR DODELE
char& DynString::operator[](int i){
    return text[i];
}

// SUBSCRIPT indeksiranje, OPERATOR PRISTUPA
char DynString::operator[](int i) const {
    return text[i];
}

bool operator==(const DynString &ds1, const DynString &ds2){
    // ako im se duzine ne poklapaju sigurno su razliciti
    if(ds1.duzina == ds2.duzina) {
        for(int i = 0; i < ds1.duzina; i++){
            if(ds1.text[i] != ds2.text[i]){
                return false;
            }
        }
    } else {
        return false;
    }

    return true;
}

bool operator!=(const DynString &ds1, const DynString &ds2){
    return !(ds1 == ds2);   /// koristim vec preklopljeni operator
}


DynString operator+(const DynString &ds1, const DynString &ds2){
    DynString temp;

    temp.duzina = ds1.duzina + ds2.duzina;

    int i = 0;
    //char *tempText = new char[ds1.duzina + ds2.duzina + 1];
    temp.text = new char[temp.duzina + 1];

    for(i = 0; i < ds1.duzina; i++){
        temp.text[i] = ds1.text[i];
    }

    for(i = 0; i < ds2.duzina; i++){
        temp.text[ds1.duzina+i] = ds2.text[i];
    }

    temp.text[temp.duzina] = '\0';
    return temp;
}

//DynString& DynString::operator+=(const DynString &ds1) {
//    int duzinaDomacina = duzina;
//
//    delete [] this->text;
//
//    this->text = new char[duzinaDomacina + ds1.duzina + 1];
//
//    for(int i = 0; i < ds1.duzina; i++){
//        this->text[duzinaDomacina+i] = ds1.text[i];
//    }
//
//    this->text[duzinaDomacina + ds1.duzina] = '\0';
//
//    return *this;
//}

DynString& DynString::operator+=(const DynString &ds1) {

    int duzinaDomacina = duzina;

    char *tempText = new char[duzinaDomacina + ds1.duzina + 1];

    for(int i = 0; i < duzina; i++){
        tempText[i] = text[i];
    }

    for(int i = 0; i < ds1.duzina; i++){
        tempText[duzinaDomacina + i] = ds1.text[i];
    }

    tempText[duzinaDomacina + ds1.duzina] = '\0';
    duzina += ds1.duzina;

    text = tempText;

    return *this;
}

ostream& operator<<(ostream& out, const DynString &ds){
    if (ds.duzina > 0) {
        out << ds.text;
    }

    return out;
}




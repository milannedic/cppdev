#ifndef VALJAK_HPP_INCLUDED
#define VALJAK_HPP_INCLUDED

#include <iostream>
#include <iomanip> //zbog setprecision funkcije koriscene u main-u

using namespace std;

#include "Krug.hpp"
#include "Pravougaonik.hpp"

// klasa valjak modeluje vezu kompozicije
// kompozicija podrazumeva upotrebu vec gotovih klasa (klasa pravougaonik i krug)
class Valjak {
    private:
        Krug Baza; // objekat-clan tipa klase Krug
        Pravougaonik M; // objekat-clan tipa klase Pravougaonik
    public:
        // objekti-clanovi B i M postavljaju se u inicijalno stanje uz pomoc konstruktora inicijalizatora :
        // operator dvotacka u ovom kontektu predstavlja konstruktor inicijalizator
        // objekat-clan B bice postavljen u inicijalno stanje pozivom konstruktora sa parametrima klase Krug
        // objekat-clan M bice postavljen u inicijalno stanje pozivom konstruktora sa parametrima klase Pravouganik
        Valjak(double rr = 2, double hh = 4) : Baza(rr), M(2 * rr * M_PI, hh) {}

        // da bi napravili valjak potrebna su 2 podatka, poluprecnik baze (krug) i visina valjka (stranica b pravougaonika)
        // poluprecnik baze valjka u stvari je poluprecnik objekta-clana B
        double getR() const{
            return Baza.getR();
        }

        // visina valjka u stvari je stranica b objekta-clana M
        double getH() const{
            return M.getB();
        }

        double getP() const{
            return 2 * Baza.getP() + M.getP();
        }

        // visinu se racuna kada se pomnozi povrsina baze sa visinom
        double getV() const{
            return Baza.getP() * getH();
        }
};

#endif // VALJAK_HPP_INCLUDED

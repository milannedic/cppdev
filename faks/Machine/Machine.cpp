#include "Machine.hpp"

/// prazan konstruktor  //nije uvek potreban ili obavezan
Machine::Machine(){
    trenutnoStanje = sC;
    value = VALUE_MAX;
}

/// konstruktor sa parametrima
//Machine::Machine(int i){
//    val = i; //
//}

/// konstruktor kopije
//Machine::Machine(const Machine &k){
    //value = k.value;
    //trenutnoStanje = k.trenutnoStanje;
//}

/// Metoda moze da se pozove iz stanja sC ili sB
/// Prebacuje automat u stanje sA
bool Machine::metodaX(){

    bool uspesno = false;

    if(trenutnoStanje == sC || trenutnoStanje == sB){
        trenutnoStanje = sA;
        uspesno = true;
    } else {
        uspesno = false;
    }

    return uspesno;
}


/// Metoda moze da se pozove iz stanja sB ili sD
/// Prebacuje automat u stanje sC (stavlja value = 80)
bool Machine::metodaY(){

    bool uspesno = false;

    if(trenutnoStanje == sB || trenutnoStanje == sD){
        trenutnoStanje = sC;
        value = 80;
        uspesno = true;
    } else {
        uspesno = false;
    }

    return uspesno;
}


/// Metoda moze da se pozove iz stanja sA
/// Prebacuje automat u stanje sB (stavlja value = 0)
bool Machine::metodaZ(){

    bool uspesno = false;

    if(trenutnoStanje == sA){
        trenutnoStanje = sB;
        value = 0;
        uspesno = true;
    } else {
        uspesno = false;
    }

    return uspesno;
}

/// Metoda moze da se pozove iz stanja sA
/// Prebacuje automat u stanje sD (stavlja value = 0)
bool Machine::metodaW(){

    bool uspesno = false;

    if(trenutnoStanje == sA){
        trenutnoStanje = sD;
        value = 0;
        uspesno = true;
    } else {
        uspesno = false;
    }

    return uspesno;
}

/// Metoda koja vraca trenutno stanje
string Machine::getCurrentState() const {
    switch(trenutnoStanje) {
        case sA: return "sA"; break;
        case sB: return "sB"; break;
        case sC: return "sC"; break;
        case sD: return "sD"; break;
    }
}


/// Metoda koja vraca value polje
int Machine::getValue() const {
    return value;
}

/// Metoda koja uvecava value za VALUE_STEP ako je ispunjen uslov
bool Machine::plus(){

    bool uspesno = false;

    /// ako se nalazim u stanju A i ako trenutna vrijednost value uvecano za VALUE_STEP
    /// ne prekoracuje VALUE_MAX
    if(trenutnoStanje == sA && value + VALUE_STEP <= VALUE_MAX){
        /// uvecavam value za zadati korak
        value += VALUE_STEP;
        uspesno = true;
    } else {
        uspesno = false;
    }

    return uspesno;
}


/// Metoda koja umanjuje value za VALUE_STEP ako je ispunjen uslov
bool Machine::minus(){

    bool uspesno = false;

    /// ako se nalazim u stanju A i ako trenutna vrijednost value umanjeno za VALUE_STEP
    /// ne prekoracuje VALUE_MIN
    if(trenutnoStanje == sA && value - VALUE_STEP >= VALUE_MIN){
        /// smanjujem value za zadati korak
        value -= VALUE_STEP;
        uspesno = true;
    } else {
        uspesno = false;
    }

    return uspesno;
}


#include <iostream>
#include "Machine.hpp"

/// Datum: 20.03.2017
/// Bezvezni sablon-zadatak... STAJEOVOSTA???

using namespace std;

int meni() {

    int izbor = 0;

    cout << "Izaberite neku od ponudjenih metoda:" << endl;
    cout << "1. metodaX()" << endl;
    cout << "2. metodaY()" << endl;
    cout << "3. metodaZ()" << endl;
    cout << "4. metodaW()" << endl;
    cout << "5. plus() // UVECAJ" << endl;
    cout << "6. minus() // UMANJI" << endl;
    cout << "7. Prikazi trenutno stanje." << endl; /// string getCurrentState() const;
    cout << "8. Prikazi trenutnu vrijednost." << endl; /// int getValue() const;
    cout << "9. Izlaz iz programa." << endl;

    cin >> izbor;

    return izbor;
}

void printInfo(const Machine &m) {
    cout << "----------------------" << endl;
    cout << "----------------- Log:" << endl;
    cout << "Value: " << m.getValue() << endl;
    cout << "Trenutno stanje: " << m.getCurrentState() << endl;
    cout << endl;
    cout << "----------------------" << endl;
}


int main()
{
    cout << endl;
    int izbor = 0;
    Machine m; /// instanciram objekat objekat klase Machine

    do {

        izbor = meni();

        switch(izbor){
            case 1:
                if(m.metodaX()) {
                    cout << "metodaX() uspjesno izvrsena!" << endl;
                } else {
                    cout << "Operacija nije izvrsena!" << endl;
                }
            break;

            case 2:
                if(m.metodaY()) {
                    cout << "metodaY() uspjesno izvrsena!" << endl;
                } else {
                    cout << "Operacija nije izvrsena!" << endl;
                }
            break;

            case 3:
                if(m.metodaZ()) {
                    cout << "metodaZ() uspjesno izvrsena!" << endl;
                } else {
                    cout << "Operacija nije izvrsena!" << endl;
                }
            break;

            case 4:
                if(m.metodaW()) {
                    cout << "metodaW() uspjesno izvrsena!" << endl;
                } else {
                    cout << "Operacija nije izvrsena!" << endl;
                }
            break;

            /// plus()
            case 5:
                if(m.plus()) {
                    cout << "plus() uspjesno izvrsena!" << endl;
                } else {
                    cout << "Uvecanje vrednosti nije izvrseno!" << endl;
                }
            break;

            /// minus()
            case 6:
                if(m.minus()) {
                    cout << "minus() operacija uspjesno izvrsena!" << endl;
                } else {
                    cout << "Umanjenje vrednosti nije izvrseno!" << endl;
                }
            break;

            case 7:
                cout << "Trenutno stanje: " << m.getCurrentState() << endl;
            break;

            case 8:
                cout << "Trenutna vrijednost: " << m.getValue() << endl;
            break;

            /// Izlazimo iz programa
            case 9:
                return 0;
            break;

            default:
            cout << "Nepoznata metoda!" << endl;
            /// Izlazimo iz programa
            return 0;
            break;
        }

        /// Prosledim objekat iz koga zelim da pokupim podatke
        printInfo(m);

    } while(izbor != 9);
    return 0;
}

#ifndef VREME_HPP_INCLUDED
#define VREME_HPP_INCLUDED
#include <iostream>

using namespace std;

class Vreme {
    private:
        int sekunde;
    public:

    //~Vreme();
    Vreme();
    Vreme(int s);
    Vreme(int s, int m);
    Vreme(int s, int m, int h);

    Vreme(const Vreme &v);

    Vreme& operator=(const Vreme&);
    Vreme& operator+=(const Vreme&);
    Vreme& operator-=(const Vreme&);

    /// ++ i -- opeatori ce isto raditi i ako ne kazem da vracaju CONST
    /// ++ i -- opeatori ce isto raditi i ako ne kazem da vracaju CONST
    /// ++ i -- opeatori ce isto raditi i ako ne kazem da vracaju CONST
    /// ++ i -- opeatori ce isto raditi i ako ne kazem da vracaju CONST
    const Vreme& operator++();          // prefiksni
    const Vreme operator++(int);        // postfiksni

    const Vreme& operator--();          // prefiksni
    const Vreme operator--(int);        // postfiksni


    friend bool operator==(const Vreme &v1, const Vreme &v2);
    friend bool operator<=(const Vreme &v1, const Vreme &v2);
    friend bool operator>=(const Vreme &v1, const Vreme &v2);
    friend bool operator!=(const Vreme &v1, const Vreme &v2);
    friend bool operator>(const Vreme &v1, const Vreme &v2);
    friend bool operator<(const Vreme &v1, const Vreme &v2);

    friend Vreme operator+(const Vreme&, const Vreme&);
    friend Vreme operator-(const Vreme&, const Vreme&);

    friend ostream& operator<<(ostream&, const Vreme&);
    friend istream& operator>>(istream &, Vreme &);


};

#endif // VREME_HPP_INCLUDED

#include "Vreme.hpp"

// Konstruktor bez parametara
Vreme::Vreme() {
    sekunde = 0;
}

Vreme::Vreme(int s){
    sekunde = s;
}

Vreme::Vreme(int s, int m){
    sekunde = s + 60 * m;
}

Vreme::Vreme(int s, int m, int h){
    sekunde = s + 60 * m + 3600 * h;
}


Vreme::Vreme(const Vreme &v){
    sekunde = v.sekunde;
}

/// v1.operator+(v2);
Vreme operator+(const Vreme &v1, const Vreme &v2){
    Vreme v(v1);
    int sek = v1.sekunde + v2.sekunde;

    v.sekunde = sek;

    return v;
}


/// v1.operator-(v2);
Vreme operator-(const Vreme &v1, const Vreme &v2){
    Vreme v(v1);
    int sek = v1.sekunde - v2.sekunde;

    v.sekunde = sek;

    return v;
}

Vreme& Vreme::operator=(const Vreme &v){
    sekunde = v.sekunde;
    return *this;
}

Vreme& Vreme::operator+=(const Vreme &v){
    sekunde += v.sekunde;
    return *this;
}

Vreme& Vreme::operator-=(const Vreme &v){
    sekunde -= v.sekunde;
    return *this;
}

/// Prefiksni, UVECAVA PA VRACA VREDNOST
const Vreme& Vreme::operator++() {
    ++sekunde;
    return *this;
}

/// Postfiksni, VRACA VREDNOST PA UVECAVA
const Vreme Vreme::operator++(int i) {
    Vreme v(sekunde);
    ++sekunde;
    return v;
}


/// Prefiksni, UMANJI PA VRACA VREDNOST
const Vreme& Vreme::operator--() {
    --sekunde;
    return *this;
}

/// Postfiksni, VRACA VREDNOST PA UMANJI
const Vreme Vreme::operator--(int i) {
    Vreme v(sekunde);
    --sekunde;
    return v;
}


bool operator==(const Vreme &v1, const Vreme &v2) {
    return v1.sekunde == v2.sekunde;
}

bool operator<=(const Vreme &v1, const Vreme &v2) {
    return v1.sekunde <= v2.sekunde;
}

bool operator>=(const Vreme &v1, const Vreme &v2) {
    return v1.sekunde >= v2.sekunde;
}

bool operator!=(const Vreme &v1, const Vreme &v2) {
    //return v1.sekunde != v2.sekunde;                  // moglo je i ovako
    return !(v1 == v2);                                 // koristimo to sto smo vec preklopili
}

bool operator>(const Vreme &v1, const Vreme &v2) {
    return v1.sekunde > v2.sekunde;
}

bool operator<(const Vreme &v1, const Vreme &v2) {
    return v1.sekunde < v2.sekunde;
}


ostream& operator<<(ostream &out, const Vreme &v){
    out << v.sekunde << endl;
}

istream& operator>>(istream &in, Vreme &v) {
    in >> v.sekunde;
    return in;
}

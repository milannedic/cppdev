#include <iostream>
#include "Vreme.hpp"

using namespace std;

int main()
{
    Vreme v1(34);
    Vreme v2(22);
    Vreme v3(100, 2, 3);

    cout << "v1 = " << v1 << endl;
    cout << "v2 = " << v2 << endl;
    cout << "===================================" << endl;
    cout << "v1 + v2 = " << (v1+v2) << endl;
    cout << "v1 - v2 = " << (v1-v2) << endl;

    v1 = v2;
    cout << "v1 nakon v1 = v2 ===> " << v1 << endl;

    v1 += v3;
    cout << "v1 += v3 ===> " << v1 << endl;

    v1 -= v3;
    cout << "v1 -= v3 ===> " << v1 << endl;

    // c++
    cout << "Postfiksni ++ ===>" << v1++ << endl;
    cout << "v1 nakon v1++ : " << v1 << endl;

    cout << endl;
    // ++c
    cout << "Prefiksni ++ ===>" << ++v1 << endl;
    cout << "v1 nakon ++v1 : " << v1 << endl;

    // c++
    //int a = 5;
    //cout << "Prefiksni ++a ===>" << ++a << endl;
    //cout << "v1 nakon ++a : " << a << endl;
    cout << "===================================" << endl;

    // c--
    cout << "Postfiksni -- ===>" << v1-- << endl;
    cout << "v1 nakon v1-- : " << v1 << endl;

    cout << endl;
    // --c
    cout << "Prefiksni -- ===>" << --v1 << endl;
    cout << "v1 nakon --v1 : " << v1 << endl;

    cout << "===================================" << endl;

    v2 = v3;
    cout << "Trenutno stanje, v1 = " << v1 << " v2 = " << v2 << endl;

    cout << " v1 == v2 ---> " << ((v1 == v2) ? "Da" : "Ne") << endl;
    cout << " v1 != v2 ---> " << ((v1 != v2) ? "Da" : "Ne") << endl;
    cout << " v1 >= v2 ---> " << ((v1 >= v2) ? "Da" : "Ne") << endl;
    cout << " v1 <= v2 ---> " << ((v1 <= v2) ? "Da" : "Ne") << endl;
    cout << " v1  < v2 ---> " << ((v1 < v2) ? "Da" : "Ne") << endl;
    cout << " v1  > v2 ---> " << ((v1 > v2) ? "Da" : "Ne") << endl;

    return 0;
}

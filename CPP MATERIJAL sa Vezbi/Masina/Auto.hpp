#ifndef AUTO_HPP_INCLUDED
#define AUTO_HPP_INCLUDED

#include"Masina.hpp"


class Auto : public Masina {


protected:

		DinString marka,model,gorivo;
		double jacina;
		static int brinstanci;

public:



		Auto(const DinString&  ds1,const DinString& ds2, const DinString& ds3,double ja=0) : marka(ds1),model(ds2),gorivo(ds3), jacina(ja) {

			cout << " Konstruktor 1 " << endl;

			brinstanci++;

		}
		Auto(const Auto& m) : marka(m.marka),jacina(m.jacina),model(m.model),gorivo(m.gorivo) {

			cout << " Konstruktor kopije" << endl;

			brinstanci++;

									}
		~Auto() {

			cout << " destruktor! " << endl;
			brinstanci--;
		}


		void printAuto() const {

			cout << " Marka: " << marka << " Model: " << model << "  Gorivo: " << gorivo << " Jacina motora: " << jacina << " brojInstanci: " << brinstanci << endl;
		}

		DinString getMarka() const {

			return marka;
		}

		DinString getGorivo() const {

			return gorivo;
		}

		DinString getModel() const {

			return model;
		}
		double getJacina() const {

			return jacina;
		}
		void setMarka(DinString ds1) {

				marka=ds1;
		}

		void setJacina(double j) {

				jacina=j;
		}

		void setGorivo(DinString ds2) {

				gorivo=ds2;
		}

		void setModel(DinString ds3) {

				model=ds3;
		}

};

#endif // AUTO_HPP_INCLUDED

#ifndef MASINA_HPP_INCLUDED
#define MASINA_HPP_INCLUDED

#include<iostream>
#include"dinstring.hpp"

using namespace std;

class Masina {


	public:

			virtual DinString getMarka() const =0;
			virtual DinString getModel() const =0;
			virtual DinString getGorivo() const =0;
			virtual double 	getJacina() const=0;
			virtual void setMarka(DinString ) =0;
			virtual void setModel(DinString ) =0;
			virtual void setGorivo(DinString ) =0;
			virtual void setJacina(double) = 0;
};

#endif // MASINA_HPP_INCLUDED

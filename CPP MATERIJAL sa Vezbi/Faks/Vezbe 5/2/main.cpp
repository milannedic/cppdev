#include "artikal.hpp"

int main()
{
    Artikal a1("Milka sa lesnikom 150gr", 142.23);
    cout << a1 << endl;

    DinString d1("Next jabuka 1l");

    Artikal a2(d1, 110);
    cout << a2 << endl;

    {
        Artikal a3(a2);
        cout << a3 << endl;
    } // kako bi se videla uloga destruktora

    cout << a2 << endl;

    /*

    // moze ukoliko se postavi za public polje
    // drugi nacin direktnog pristupa statickom polju naziv_klase::naziv_staickog_polja
    cout << "BROJ INSTANCI KLASE ARTIKAL JE: " << Artikal::instanci << endl << endl;
    Artikal::instanci = 6;
    cout << "BROJ INSTANCI KLASE ARTIKAL JE: " << Artikal::instanci << endl;

    */

    return 0;
}

#include "Valjak.hpp"

int main()
{
    // pravljenje instance uz pomoc podrazumevanih vrednosti
    Krug k1;
    // pravljenje instance uz pomoc prosledjene vrednosti
    Krug k2(3);
    // pravljenje instance uz pomoc podrazumevanih vrednosti
    Pravougaonik p1;
    // pravljenje instance uz pomoc prosledjene vrednosti
    Pravougaonik p2(1,8);
    // pravljenje instance uz pomoc podrazumevanih vrednosti
    Valjak v1;
    // pravljenje instance uz pomoc prosledjene vrednosti
    Valjak v2(2,4);

    // kada se pre ispisa double ili float vrednosti cout prosledi fixed
    // teramo cout da prikaze nebitne cifre
    // na primer cout << 3.0000 prikazace samo 3, a cout << fixed << 3.000 prikazace 3.000
    // funkciju setprecision koristimo kada zelimo da odredimo broj potrebnih mesta
    // na primer cout << setprecision(2) << 3.123 prikazace 3.12
    cout << fixed << setprecision(2);

    cout << "Instanca k1" << endl;
    cout << "Povrsina kruga je: " << k1.getP() << " ,a obim je: " << k1.getO() << endl << endl;

    cout << "Instanca k2" << endl;
    cout << "Povrsina kruga je: " << k2.getP() << " ,a obim je: " << k2.getO() << endl << endl;

    cout << "Instanca p1" << endl;
    cout << "Povrsina pravougaonika je: " << p1.getP() << " ,a obim je: " << p1.getO() << endl << endl;

    cout << "Instanca p2" << endl;
    cout << "Povrsina pravougaonika je: " << p2.getP() << " ,a obim je: " << p2.getO() << endl << endl;

    cout << "Instanca v1" << endl;
    cout << "Povrsina valjka je: " << v1.getP() << " ,a zapremina je: " << v1.getV() << endl << endl;

    cout << "Instanca v2" << endl;
    cout << "Povrsina valjka je: " << v2.getP() << " ,a zapremina je: " << v2.getV() << endl << endl;

    return 0;
}

#include "Televizor.hpp"

    // Konstruktor klase Televizor
    Televizor::Televizor(){
        stanje = ISKLJUCEN;
        zvuk = 0;
        kanal = 1;
    }

    // konstruktor kopije
    Televizor::Televizor(const Televizor &tv) {
        stanje = tv.stanje;
        zvuk = tv.zvuk;
        kanal = tv.kanal;
    }

    // Metoda za promenu stanja u UKLJUCEN
    bool Televizor::ukljuci() {
        bool uspeh = false;

        // mogu da ga UKLJUCIM samo ako je trenutno ISKLJUECN
        if(stanje == ISKLJUCEN) {
            // samo stanje prebacujem u ukljucen
            stanje = UKLJUCEN;

            uspeh = true;
        } else {
            uspeh = false;
        }

        return uspeh;
    }

    // Metoda za promenu stanja u ISKLJUCEN
    bool Televizor::iskljuci() {
        bool uspeh = false;

        // mogu da ga UKLJUCIM samo ako je trenutno ISKLJUECN
        if(stanje == UKLJUCEN) {
            stanje = ISKLJUCEN;
            // vracam zvuk i kanal u odgovarajuce stanje
            zvuk = 0;
            kanal = 1;

            uspeh = true;
        } else {
            uspeh = false;
        }

        return uspeh;
    }

    // Metoda za promenu stanja u POKVAREN
    bool Televizor::pokvari() {
        bool uspeh = false;

        // mogu da ga UKLJUCIM samo ako je trenutno ISKLJUECN
        if(stanje == UKLJUCEN || stanje == ISKLJUCEN) {
            stanje = POKVAREN;
            // vracam zvuk i kanal u odgovarajuce stanje
            zvuk = -1;
            kanal = -1;

            uspeh = true;
        } else {
            uspeh = false;
        }

        return uspeh;
    }

    // Metoda za promenu stanja u ISKLJUCEN, nakon popravke
    bool Televizor::popravi() {
        bool uspeh = false;

        // mogu da ga UKLJUCIM samo ako je trenutno ISKLJUECN
        if(stanje == POKVAREN) {
            stanje = ISKLJUCEN;
            // vracam zvuk i kanal u odgovarajuce stanje
            zvuk = 0;
            kanal = 1;

            uspeh = true;
        } else {
            uspeh = false;
        }

        return uspeh;
    }


    //
    bool Televizor::pojacajZvuk() {
        bool uspeh = false;

        // mogu da ga UKLJUCIM samo ako je trenutno ISKLJUECN
        if(stanje == UKLJUCEN && zvuk + ZVUK_STEP <=  ZVUK_MAX) {
            zvuk++;
            uspeh = true;
        } else {
            uspeh = false;
        }

        return uspeh;
    }


    //
    bool Televizor::smanjiZvuk() {
        bool uspeh = false;

        // mogu da ga UKLJUCIM samo ako je trenutno ISKLJUECN
        if(stanje == UKLJUCEN && zvuk - ZVUK_STEP >=  ZVUK_MIN) {
            zvuk--;
            uspeh = true;
        } else {
            uspeh = false;
        }

        return uspeh;
    }

    bool Televizor::sledeciKanal() {
        bool uspeh = false;

        // mogu da ga UKLJUCIM samo ako je trenutno ISKLJUECN
        if(stanje == UKLJUCEN && kanal + KANAL_STEP <=  KANAL_MAX) {
            kanal++;
            uspeh = true;
        } else {
            uspeh = false;
        }

        return uspeh;
    }

    bool Televizor::prethodniKanal() {
        bool uspeh = false;

        // mogu da ga UKLJUCIM samo ako je trenutno ISKLJUECN
        if(stanje == UKLJUCEN && kanal - KANAL_STEP >=  KANAL_MIN) {
            kanal--;
            uspeh = true;
        } else {
            uspeh = false;
        }

        return uspeh;
    }

    string Televizor::getStanje() const {
        switch(stanje){
            case ISKLJUCEN: return "ISKLJUCEN"; break;
            case UKLJUCEN: return "UKLJUCEN"; break;
            case POKVAREN: return "POKVAREN"; break;
        }
    }
    int Televizor::getZvuk() const {
        return zvuk;
    }
    int Televizor::getKanal() const {
        return kanal;
    }

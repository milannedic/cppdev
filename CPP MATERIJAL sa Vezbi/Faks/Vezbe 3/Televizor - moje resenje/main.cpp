#include <iostream>
#include "Televizor.hpp"
using namespace std;

int meni() {
    cout << "1. ukljuci" << endl;
    cout << "2. iskljuci" << endl;
    cout << "3. pokvari" << endl;
    cout << "4. popravi" << endl;
    cout << "5. pojacajZvuk" << endl;
    cout << "6. smanjiZvuk" << endl;
    cout << "7. sledeciKanal" << endl;
    cout << "8. prethodniKanal" << endl;
    cout << "9. Prikazi stanje" << endl;
    cout << "10. Kraj rada programa" << endl;

    // da ne izbaci neko djubre iz memorije
    int izbor = 0;
    cin >> izbor;

    return izbor;
}

void ispisInfo(const Televizor &tv) {
    cout << "----------------------" << endl;
    cout << "Trenutno stanje: " << tv.getStanje() << endl;
    cout << "Zvuk: " << tv.getZvuk() << endl;
    cout << "Kanal: " << tv.getKanal() << endl;
    //cout << endl;
    cout << "----------------------" << endl;
}
int main()
{
    cout << endl;
    int izbor = 0;
    Televizor tv;

    // treba da ga napravim iako ne treba da ga koristim
    // samo da pokazem da znam da napravim... GLUPOST!
    Televizor tv2(tv);

    do {
        izbor = meni();

        switch(izbor){
            case 1:
                if(tv.ukljuci()){
                    cout << "TV uspesno ukljucen!" << endl;
                } else {
                    cout << "TV nije ukljucen!" << endl;
                }
            break;

            case 2:
                if(tv.iskljuci()){
                    cout << "TV uspesno iskljucen!" << endl;
                } else {
                    cout << "TV nije iskljucen!" << endl;
                }
            break;

            case 3:
                if(tv.pokvari()){
                    cout << "TV pokvaren!" << endl;
                } else {
                    cout << "Nije uspesna operacija!" << endl;
                }
            break;

            case 4:
                if(tv.popravi()){
                    cout << "TV popravljen!" << endl;
                } else {
                    cout << "Nije uspesna operacija!" << endl;
                }
            break;

            case 5:
                if(tv.pojacajZvuk()){
                    cout << "Uspesno pojacan zvuk!" << endl;
                } else {
                    cout << "Nije uspesna operacija!" << endl;
                }
            break;

            case 6:
                if(tv.smanjiZvuk()){
                    cout << "Uspesno smanjen zvuk!" << endl;
                } else {
                    cout << "Nije uspesna operacija!" << endl;
                }
            break;

            case 7:
                if(tv.sledeciKanal()){
                    cout << "Uspesno uvecan kanal!" << endl;
                } else {
                    cout << "Nije uspesna operacija!" << endl;
                }
            break;

            case 8:
                if(tv.prethodniKanal()){
                    cout << "Uspesno umanjen kanal!" << endl;
                } else {
                    cout << "Nije uspesna operacija!" << endl;
                }
            break;

            case 9:
                cout << "Stanje je: " << tv.getStanje() << endl;
            break;

            case 10:
                cout << "Uspesno ste izasli iz programa! " << endl;
                return 0;
            break;

            default :
                cout << "Nepoznata komanda!" << endl;
            break;
        }

        ispisInfo(tv);
        //cout << "Izbor je: " << izbor << endl;
    } while(izbor != 10);
    return 0;
}

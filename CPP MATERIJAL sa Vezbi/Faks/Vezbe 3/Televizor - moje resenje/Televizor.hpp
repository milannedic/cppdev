#ifndef TELEVIZOR_HPP_INCLUDED
#define TELEVIZOR_HPP_INCLUDED

#define ZVUK_STEP 1
#define ZVUK_MIN 0
#define ZVUK_MAX 20

#define KANAL_STEP 1
#define KANAL_MIN 1
#define KANAL_MAX 5

#include <iostream>
using namespace std;

#endif // TELEVIZOR_HPP_INCLUDED

enum Stanje_televizora {ISKLJUCEN, UKLJUCEN, POKVAREN};

class Televizor{
    private:
    Stanje_televizora stanje;
    int zvuk; // u intervalu 0 - 20, korak je 1, zabraniti izlazak iz opsega
    int kanal; // opseg 1 - 5 , korak 1, ZABRANITI IZLAZAK IZ OPSEGA

    public:

    // deklaracije za prazan konstruktor
    Televizor();
    // deklaracija konstruktora kopije
    Televizor (const Televizor &tv);

    // metode za manipulaciju stanjem televizora
    bool ukljuci();
    bool iskljuci();
    bool popravi();
    bool pokvari();

    // metode za manipulaciju zvuka
    bool pojacajZvuk();
    bool smanjiZvuk();

    // manipulacija kanalom
    bool sledeciKanal();
    bool prethodniKanal();

    // pomocne funkcije, getteri
    string getStanje() const;
    int getZvuk() const;
    int getKanal() const;



};

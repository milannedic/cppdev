#include "Kocka.hpp"

// prazan konstruktor  //nije uvek potreban ili obavezan
Kocka::Kocka(){
    val = 1; //bilo koja vrednost, cesto se i naglasi
}

// konstruktor sa parametrima
Kocka::Kocka(int i){
    val = i; //
}

// konstruktor kopije
Kocka::Kocka(const Kocka &k){
    val = k.val;
}

void Kocka::baci(){
    // formula po kojoj se koristi rand() funkcija je rand()%X+Y
    // gde X predstavlja gornju granicu, a Y donju
    val = rand() % 6 + 1; //deljenje po modulu 6 //simulira bacanje kocke
}

int Kocka::getVrednost() const{
    return val;
}


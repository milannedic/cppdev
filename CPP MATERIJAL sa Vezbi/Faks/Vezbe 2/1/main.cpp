 #include "Kocka.hpp"

// deklarisanje konstante
#define KRAJ_RADA 3

int meni(){
    cout << "Izaberite operaciju" << endl;
    cout << "1. Baci kocku" << endl;
    cout << "2. Prikazi kocku" << endl;
    cout << "3. Kraj" << endl;
    cout << "Operacija-> ";
    int n;
    cin >> n;
    return n;
}

int main()
{
    // prazan konstruktor
    Kocka k1;

    // konstruktor sa parametrima
    // nije implementirana logika ako korisnik prosledi vrednost manju od 1 ili vecu od 6
    // kocka za igru moze da ima samo vrednosti 1, 2, 3, 4, 5, 6 (broj tackica)
    Kocka k2(3);

    // konstruktor kopije
    // konstruktor kopije ocekuje da mu se kao parametar prosledi podatak (promenljiva) tipa te klase
    Kocka k3(k2);

    int n;

    do{
        n = meni();

        switch(n){
            case 1: // baci
                k1.baci();
                cout << "Kocka je bacena" << endl;
                break;
            case 2: // prikazi
                cout << "Vrednost kocke je: " << k1.getVrednost() << endl;
                break;
            case 3: // kraj rada programa
                cout << "Kraj rada programa " << endl;
                break;
            default:
                cout << "Nepostojeca operacija" << endl;
                break;
        }
    }while(n != KRAJ_RADA);

    return 0;
}

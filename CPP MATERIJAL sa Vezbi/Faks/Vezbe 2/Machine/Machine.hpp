#ifndef MACHINE_HPP_INCLUDED
#define MACHINE_HPP_INCLUDED

#include <iostream>
using namespace std;

#define VALUE_MIN 0
#define VALUE_MAX 80
#define VALUE_STEP 20

enum currentState {sA, sB, sC, sD};

class Machine {

    private:
    currentState trenutnoStanje;
    int value;

    public:
    Machine();

    bool metodaX();
    bool metodaY();
    bool metodaZ();
    bool metodaW();

    bool plus();
    bool minus();

    string getCurrentState() const;
    int getValue() const;

};

#endif // MACHINE_HPP_INCLUDED

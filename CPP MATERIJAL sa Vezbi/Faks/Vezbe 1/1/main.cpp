#include <iostream>
using namespace std;

int main()
{
    int pom, suma = 0;

    do {
        cout << "Unosite broj koji biste da dodate u sumu (ili 0 za izlaz): ";
        cin >> pom;
        suma += pom;
        cout << "Trenutna suma je: " << suma << endl;
    } while(pom != 0);

    cout << "Konacna suma je: " << suma << endl;

    return 0;
}

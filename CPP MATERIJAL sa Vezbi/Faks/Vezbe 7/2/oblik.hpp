#ifndef OBLIK_DEF
#define OBLIK_DEF

#include "kvadrat.hpp"
#include "krug.hpp"


// zasebna klasa za sebe i ne nasledjuje nikoga
// ona mi sluzi zapravo samo da primjenim kompoziciju



class Oblik {

    private:
        Kvadrat A;
        Krug B;

    public:
        Oblik() : A(6), B(3) {}
        Oblik(double aa) : A(aa), B(aa / 2){}         // poluprecnik kruga je polovina stranice kvadrata
        Oblik(const Oblik& o) : A(o.A), B(o.B){}

	// ovo su njegove neke metode, koje nisu implementirane iz neke apstraktne klase
	// mogli su biti i drugacije nazvani 
        double getP() const {
            return A.getP() - B.getP();
        }

        double getO() const {
            return B.getO() + A.getO();
        }

};

#endif


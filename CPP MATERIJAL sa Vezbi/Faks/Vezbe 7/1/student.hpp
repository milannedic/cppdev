#ifndef STUDENT_DEF
#define STUDENT_DEF

#include "osoba.hpp"

class Student : public Osoba {
	protected:
		int brojIndeksa;
	
	public:
		// ime(s1), prezime(s2) ovo ne treba da radim u nasledjivanju.
		Student(const char* s1 = "", const char* s2 = "", int i = 0) : Osoba(s1, s2), brojIndeksa(i) {
			cout << "Student: Konstruktor 1." << endl;
		}
		
		// Osoba o("Pera", "Peric");
		// Student s(o, 6);
		
		Student(const DinString& ds1, const DinString& ds2, int i) : Osoba(ds1, ds2), brojIndeksa(i) {
			cout << "Student: Konstruktor 2." << endl;
		}
		
		Student(const Osoba& os, int i) : Osoba(os), brojIndeksa(i) {
			cout << "Student: Konstruktor 3." << endl;
		}
		
		// Osoba((Osoba)s...) // castovanje "iscupa" Osobu iz objekta s koji je Student.
		// Fakticni objekat drugog tipa kastujes na neki koji je sadrzan u ovom.
		Student(const Student& s) : Osoba((Osoba)s), brojIndeksa(s.brojIndeksa) {
			cout << "Student: Konstruktor 4." << endl;
		}
		
		// Zasto ovako??
		~Student() {
			cout << "Student: Destruktor." << endl;
		}
		
		void predstaviSe() const {
			Osoba::predstaviSe();
			cout << "Broj mog indeksa je " << brojIndeksa << "." << endl;
		}
};

#endif

#include "phdstudent.hpp"

int main(){

    const char *s1 = "Petar";
    const char *s2 = "Petrovic";
    const char *s3 = "Jovan";
    const char *s4 = "Jovanovic";

    cout << "*** Kreiranje objekata ds1, ds2, ds3 i ds4." << endl;

    DinString ds1(s1), ds2(s2), ds3(s3), ds4(s4);


    cout << endl << "*** Kreiranje objekata os1, os2 i os3." << endl;

    Osoba os1(s1,s2), os2(ds3,ds4), os3(os2);


    cout << endl << "*** Kreiranje objekata st1, st2, st3 i st4." << endl;

    Student st1(s1, s2, 1234), st2(ds1, ds2, 1234), st3(os2, 1234), st4(st2);


    cout << endl << "*** Kreiranje objekata phds1, phds2, phds3 i phds4." << endl;

    PhDStudent phds1(s1, s2, 1234, 8.56), phds2(ds1, ds2, 1234, 8.56), phds3(os3, 1234, 8.77), phds4(st3, 8.77);


    cout << endl << "*** Predstavljenje." << endl;

    os1.predstaviSe();
    os2.predstaviSe();
    os3.predstaviSe();

    st1.predstaviSe();
    st2.predstaviSe();
    st3.predstaviSe();
    st4.predstaviSe();

    phds1.predstaviSe();
    phds2.predstaviSe();
    phds3.predstaviSe();
    phds4.predstaviSe();

    cout << endl << "*** Unistavanje objekata." << endl;

    return 0;

}
